# Set for local builds only
%global disable_toolsets  0

%ifarch i686
# no debug package for the i686 because oom on i686 with debuginfos
%global debug_package %{nil}
%endif

%{lua:
function dist_to_rhel_minor(str, start)
  match = string.match(str, ".module%+el8.%d+")
  if match then
     return string.sub(match, 13)
  end
  match = string.match(str, ".el8_%d+")
  if match then
     return string.sub(match, 6)
  end
  match = string.match(str, ".el8")
  if match then
     return 5
  end
  match = string.match(str, ".el9")
  if match then
     return 4
  end
  return -1
end}

%global rhel_minor_version %{lua:print(dist_to_rhel_minor(rpm.expand("%dist")))}

%global system_nss        1
%global bundle_nss        0

%if 0%{?rhel} >= 8
  %if %{rhel_minor_version} < 2
%global bundle_nss        1
  %endif
%endif

%if 0%{?rhel} == 6
%global system_nss        0
%endif

%define use_bundled_ffi   0

%define use_bundled_python_2 1
%define use_bundled_python_3 1

%if 0%{?rhel} >= 8
%define use_bundled_python_2 1
%define use_bundled_python_3 0
%endif

%if 0%{?rhel} == 7
%define use_bundled_python_2 0
%define use_bundled_python_3 0
%endif

%if 0%{?flatpak:1}
%define use_bundled_python_2 1
%endif

# we need python2 because of icu data gen
%define use_bundled_python_2 0
# Don't use system hunspell for now
%global system_hunspell   0
%if 0%{?rhel} >= 8
%global use_llvmts        0
%else
%global use_llvmts        1
%endif

%global system_ffi        1
%if 0%{?rhel} < 8
%global use_dts           1
%endif

%global use_rustts        1
%global dts_version       8
%global rust_version         1.41
%global rust_toolset_version 1.41
%global llvm_version      7.0
%if 0%{?rhel} >= 8
%global llvm_version      6.0
%endif

%if 0%{?disable_toolsets}
%global use_rustts        0
%global use_dts           0
%global use_llvmts        0
%endif

# Use system cairo?
%global system_cairo      0

# Use system libvpx?
%global system_libvpx     0

# Use system libicu?
%global system_libicu     0

# Big endian platforms
%ifarch ppc64 s390x
# Javascript Intl API is not supported on big endian platforms right now:
# https://bugzilla.mozilla.org/show_bug.cgi?id=1322212
%global big_endian        1
%endif

# Hardened build?
%global hardened_build    1

%global system_jpeg       1

%ifarch %{ix86} x86_64
%global run_tests         0
%else
%global run_tests         0
%endif

# Build as a debug package?
%global debug_build       0

%define thunderbird_app_id      \{3550f703-e582-4d05-9a08-453d09bdfdc6\}
# Minimal required versions
%global cairo_version 1.13.1
%global freetype_version 2.1.9
%if %{?system_libvpx}
%global libvpx_version 1.4.0
%endif

%if 0%{?system_nss}
%global nspr_version 4.25
# NSS/NSPR quite often ends in build override, so as requirement the version
# we're building against could bring us some broken dependencies from time to time.
#%%global nspr_build_version %%(pkg-config --silence-errors --modversion nspr 2>/dev/null || echo 65536)
%global nspr_build_version %{nspr_version}
%global nss_version 3.53.1
#%%global nss_build_version %%(pkg-config --silence-errors --modversion nss 2>/dev/null || echo 65536)
%global nss_build_version %{nss_version}
%endif


%define bundled_python_version_2 2.7.13
%define bundled_python_version_3 3.6.8
%define use_bundled_openssl     0
%define use_bundled_nodejs      0
%define use_bundled_yasm        0

%if 0%{?rhel} >= 8
  %if %{rhel_minor_version} <= 2
%define use_bundled_nodejs      1
  %endif
%endif

%if 0%{?rhel} == 7
%define use_bundled_nodejs      1
%define use_bundled_yasm        1
%endif

%define avoid_bundled_rebuild   0

%define gtk3_install_path %{mozappdir}/bundled

# We could use %%include, but in %%files, %%post and other sections, but in these
# sections it could lead to syntax errors about unclosed %%if. Work around it by
# using the following macro
%define include_file() %{expand:%(cat '%1')}

%global mozappdir     %{_libdir}/%{name}
%global mozappdirdev  %{_libdir}/%{name}-devel-%{version}
%global langpackdir   %{mozappdir}/distribution/extensions
%global tarballdir    %{name}-%{version}
%global mozappfeatures %{mozappdir}/features/*.xpi
#global pre_tag       alpha

%global official_branding       1
%global build_langpacks         1
Summary:        Mozilla Thunderbird mail/newsgroup client
Name:           thunderbird
Version:        78.13.0
Release:        1%{?dist}
URL:            http://www.mozilla.org/projects/thunderbird/
License:        MPLv1.1 or GPLv2+ or LGPLv2+
Group:          Applications/Internet

%if 0%{?rhel} == 7
ExcludeArch:    s390 ppc
%endif

# We can't use the official tarball as it contains some test files that use
# licenses that are rejected by Red Hat Legal.
# The official tarball has to be always processed by the process-official-tarball
# script.
# Link to official tarball: https://archive.mozilla.org/pub/thunderbird/releases/%%{version}%%{?pre_version}/source/thunderbird-%%{version}%%{?pre_version}.source.tar.xz
Source0:        thunderbird-%{version}%{?pre_version}.processed-source.tar.xz
%if %{build_langpacks}
Source1:        thunderbird-langpacks-%{version}%{?ext_version}-20210804.tar.xz
# Locales for lightning
%endif
Source2:        cbindgen-vendor-0.14.3.tar.xz
Source3:        get-calendar-langpacks.sh
Source4:        process-official-tarball

Source10:       thunderbird-mozconfig
Source20:       thunderbird.desktop
Source21:       thunderbird.sh.in
Source24:       mozilla-api-key
Source27:       google-api-key
Source28:       node-stdout-nonblocking-wrapper
Source301:      yasm-1.2.0-3.el5.src.rpm
Source303:      libffi-3.0.13-18.el7_3.src.rpm
Source304:      nodejs-10.21.0-5.fc32.src.rpm
Source305:      openssl-1.0.2k-19.6.bundle.el7_7.src.rpm
Source601:      thunderbird-redhat-default-prefs.js.rhel6
Source701:      thunderbird-redhat-default-prefs.js.rhel7

## Firefox patches

Source403:      nss-3.53.1-3.fc32.src.rpm
Source401:      nss-setup-flags-env.inc
Source402:      nspr-4.25.0-1.el8_0.src.rpm 
#Python
%if 0%{?use_bundled_python_2}
Source100:      https://www.python.org/ftp/python/%{bundled_python_version_2}/Python-%{bundled_python_version_2}.tar.xz
%endif
%if 0%{?use_bundled_python_3}
Source101:      https://www.python.org/ftp/python/%{bundled_python_version_3}/Python-%{bundled_python_version_3}.tar.xz
%endif
# Build patches
Patch1000:      python-2.7.patch
# workaround for https://bugzilla.redhat.com/show_bug.cgi?id=1699374
Patch1001:      build-ppc64le-inline.patch
Patch1002:      python-2.7-gcc8-fix.patch
Patch1003:      python-missing-utimensat.patch
Patch1004:      build-icu-make.patch
Patch1006:      D89554-autoconf1.diff
Patch1007:      D94538-autoconf2.diff
# workaround for https://bugzilla.redhat.com/show_bug.cgi?id=1699374
Patch4:         build-mozconfig-fix.patch
Patch6:         build-nss-version.patch
Patch7:         firefox-debugedits-error.patch

# Fedora/RHEL specific patches
Patch215:        firefox-enable-addons.patch
Patch219:        rhbz-1173156.patch
Patch224:        mozilla-1170092.patch
#ARM run-time patch
Patch231:        firefox-pipewire.patch
Patch232:        firefox-rhel6-hugepage.patch
Patch233:        firefox-rhel6-nss-tls1.3.patch
Patch234:        rhbz-1821418.patch
Patch235:        firefox-pipewire-0-3.patch
Patch236:        fedora-shebang-build.patch
Patch237:        disable-openpgp-in-thunderbird.patch
Patch238:        firefox-glibc-dynstack.patch

# Upstream patches
Patch402:        mozilla-1196777.patch

Patch501:        python-encode.patch
Patch503:        mozilla-s390-context.patch
Patch505:        mozilla-bmo1005535.patch
Patch506:        mozilla-bmo1504834-part1.patch
Patch507:        mozilla-bmo1504834-part2.patch
Patch508:        mozilla-bmo1504834-part3.patch
Patch509:        mozilla-bmo1504834-part4.patch
Patch510:        mozilla-bmo1554971.patch
Patch511:        mozilla-bmo1602730.patch
Patch512:        mozilla-bmo849632.patch
Patch513:        mozilla-bmo998749.patch
Patch514:        mozilla-s390x-skia-gradient.patch
Patch515:        mozilla-bmo1626236.patch
Patch516:        D87019-thin-vec-big-endian.diff


%if %{?system_nss}
%if !0%{?bundle_nss}
BuildRequires:  pkgconfig(nspr) >= %{nspr_version}
BuildRequires:  pkgconfig(nss) >= %{nss_version}
BuildRequires:  nss-static >= %{nss_version}
%endif
%endif
%if %{?system_cairo}
BuildRequires:  pkgconfig(cairo) >= %{cairo_version}
%endif
BuildRequires:  pkgconfig(libpng)
BuildRequires:  xz
BuildRequires:  libXt-devel
BuildRequires:  mesa-libGL-devel
Requires:       liberation-fonts-common
Requires:       liberation-sans-fonts
%if %{?system_jpeg}
BuildRequires:  libjpeg-devel
%endif
BuildRequires:  zip
BuildRequires:  bzip2-devel
BuildRequires:  pkgconfig(zlib)
BuildRequires:  pkgconfig(gtk+-2.0)
BuildRequires:  krb5-devel
BuildRequires:  pkgconfig(pango)
BuildRequires:  pkgconfig(freetype2) >= %{freetype_version}
BuildRequires:  pkgconfig(xt)
BuildRequires:  pkgconfig(xrender)
%if %{?system_hunspell}
BuildRequires:  hunspell-devel
%endif
BuildRequires:  pkgconfig(libstartup-notification-1.0)
BuildRequires:  pkgconfig(libnotify)
BuildRequires:  pkgconfig(dri)
BuildRequires:  pkgconfig(libcurl)
BuildRequires:  dbus-glib-devel
%if %{?system_libvpx}
BuildRequires:  libvpx-devel >= %{libvpx_version}
%endif
BuildRequires:  m4
BuildRequires:  pkgconfig(libpulse)

%if 0%{?use_dts}
BuildRequires:  devtoolset-%{dts_version}-gcc-c++
BuildRequires:  devtoolset-%{dts_version}-gcc
BuildRequires:  devtoolset-%{dts_version}-binutils
BuildRequires:  devtoolset-%{dts_version}-libatomic-devel
%if 0%{?use_llvmts}
BuildRequires:  llvm-toolset-%{llvm_version}
BuildRequires:  llvm-toolset-%{llvm_version}-llvm-devel
%endif
%endif

BuildRequires:  scl-utils
BuildRequires:  findutils


%if 0%{?rhel} >= 8
BuildRequires:  cargo
BuildRequires:  rust >= %{rust_version}
BuildRequires:  llvm >= %{llvm_version}
BuildRequires:  llvm-devel >= %{llvm_version}
BuildRequires:  clang >= %{llvm_version}
BuildRequires:  clang-devel >= %{llvm_version}
BuildRequires:  rustfmt >= %{rust_version}
BuildRequires:  python3
BuildRequires:  nodejs >= 10.21
%else
%if 0%{?use_rustts}
BuildRequires:  rust-toolset-%{rust_toolset_version}
%endif
%if 0%{?rhel} == 7
#BuildRequires:  rh-nodejs12
%endif
%if 0%{?use_llvmts}
BuildRequires:  llvm-toolset-%{llvm_version}
BuildRequires:  llvm-toolset-%{llvm_version}-llvm-devel
%endif
%endif

%if ! 0%{?use_bundled_yasm}
BuildRequires:  yasm
%endif


%if 0%{?use_bundled_python_2}
# Needed for Python in RHEL6
BuildRequires:  openssl-devel
%endif

%if 0%{?rhel} >= 8
  %if %{rhel_minor_version} >= 3
BuildRequires:  pkgconfig(libpipewire-0.3)
  %else
BuildRequires:  pipewire-devel
  %endif
%endif

BuildRequires:        gtk3-devel
BuildRequires:        glib2-devel

# Bundled nss/nspr requirement
%if 0%{?bundle_nss}
BuildRequires:    nss-softokn
BuildRequires:    sqlite-devel
BuildRequires:    zlib-devel
BuildRequires:    pkgconfig
BuildRequires:    gawk
BuildRequires:    psmisc
BuildRequires:    perl-interpreter
BuildRequires:    gcc-c++
BuildRequires:    xmlto
%endif
#RHEL9
BuildRequires:    perl-interpreter

Requires:       mozilla-filesystem
Requires:       p11-kit-trust
%if %{?system_nss}
%if !0%{?bundle_nss}
Requires:       nspr >= %{nspr_build_version}
Requires:       nss >= %{nss_build_version}
%endif
%endif

BuildRequires:  desktop-file-utils
BuildRequires:  system-bookmarks
Requires:       redhat-indexhtml
#for the python2
BuildRequires:  pkgconfig(sqlite3)


%if %{?run_tests}
BuildRequires:  xorg-x11-server-Xvfb
%endif

%if %{?system_ffi}
  %if !%{use_bundled_ffi}0
BuildRequires:  pkgconfig(libffi)
  %endif
%endif

%if %{?use_bundled_nodejs}
%if !0%{?use_bundled_python_3}
BuildRequires: python3-devel
%endif
BuildRequires: zlib-devel
#BuildRequires: brotli-devel
#BuildRequires: gcc >= 4.9.4
#BuildRequires: gcc-c++ >= 4.9.4
BuildRequires: chrpath
BuildRequires: libatomic
BuildRequires: openssl-devel
%endif

%if 0%{?big_endian}
BuildRequires:  icu
%endif

Obsoletes:      thunderbird-lightning
# ==================================================================================
# Bundled libraries
Provides: bundled(angle)
Provides: bundled(cairo)
Provides: bundled(graphite2)
Provides: bundled(harfbuzz)
Provides: bundled(ots)
Provides: bundled(sfntly)
Provides: bundled(skia)
Provides: bundled(thebes)
Provides: bundled(WebRender)
Provides: bundled(audioipc-2)
Provides: bundled(ffvpx)
Provides: bundled(kissfft)
Provides: bundled(libaom)
Provides: bundled(libcubeb)
Provides: bundled(libdav1d)
Provides: bundled(libjpeg)
Provides: bundled(libmkv)
Provides: bundled(libnestegg)
Provides: bundled(libogg)
Provides: bundled(libopus)
Provides: bundled(libpng)
Provides: bundled(libsoundtouch)
Provides: bundled(libspeex_resampler)
Provides: bundled(libtheora)
Provides: bundled(libtremor)
Provides: bundled(libvorbis)
Provides: bundled(libvpx)
Provides: bundled(libwebp)
Provides: bundled(libyuv)
Provides: bundled(mp4parse-rust)
Provides: bundled(mtransport)
Provides: bundled(openmax_dl)
Provides: bundled(double-conversion)
Provides: bundled(brotli)
Provides: bundled(fdlibm)
Provides: bundled(freetype2)
Provides: bundled(libmar)
Provides: bundled(woff2)
Provides: bundled(xz-embedded)
Provides: bundled(zlib)
Provides: bundled(expat)
Provides: bundled(msgpack-c)
Provides: bundled(libprio)
Provides: bundled(rlbox_sandboxing_api)
Provides: bundled(sqlite3)

#TB third party libs
Provides: bundled(libgcrypt)
Provides: bundled(libgpg-error)
Provides: bundled(libotr)

# In case we enable MOZ_OPENPGP add following:
#Provides: bundled(botan)
#Provides: bundled(bzip2)
#Provides: bundled(json-c)
#Provides: bundled(rnp)

%description
Mozilla Thunderbird is a standalone mail and newsgroup client.


%prep
echo "Build environment"
echo "dist                  %{?dist}"
echo "RHEL 8 minor version: %{rhel_minor_version}"
echo "use_bundled_ffi       %{?use_bundled_ffi}"
echo "use_bundled_python_2  %{?use_bundled_python_2}"
echo "use_bundled_python_3  %{?use_bundled_python_3}"
echo "bundle_nss            %{?bundle_nss}"
echo "system_nss            %{?system_nss}"
echo "use_rustts            %{?use_rustts}"
echo "use_bundled_nodejs    %{?use_bundled_nodejs}"
echo "use_bundled_openssl   %{?use_bundled_openssl}"
echo "use_bundled_yasm      %{?use_bundled_yasm}"


%if 0%{?use_bundled_python_2}
%setup -q -T -c -n python2 -a 100
%patch1000 -p0 -b .build
%patch1002 -p0 -b .gcc8
%endif
%if 0%{?use_bundled_python_3}
%setup -q -T -c -n python3 -a 101
%endif
%setup -q -n %{tarballdir}

# Firefox patches
%patch7 -p1 -b .debugedits-error
%ifarch %{ix86} %{arm} ppc
# binary check fails OOM on 32bit arches
%endif

%patch4  -p1 -b .build-mozconfig-fix
#%patch6  -p1 -b .nss-version

# Fedora patches
%patch215 -p1 -b .addons
%patch219 -p1 -b .rhbz-1173156
%patch224 -p1 -b .1170092
%if 0%{?rhel} >= 8
  %if %{rhel_minor_version} >= 3
%patch235 -p1 -b .pipewire-0-3
  %else
%patch231 -p1 -b .pipewire
  %endif
%endif

%patch236 -p1 -b .fedora-shebang-build
%patch237 -p1 -b .disable-openpgp-in-thunderbird
%patch238 -p1 -b .firefox-glibc-dynstack

%patch234 -p1 -b .rhbz-1821418

%patch402 -p1 -b .1196777

# Patch for big endian platforms only
%if 0%{?big_endian}
%endif

# Thunderbird patches
%patch501 -p1 -b .python-encode
%patch503 -p1 -b .mozilla-s390-context
%patch505 -p1 -b .mozilla-bmo1005535
%patch506 -p1 -b .mozilla-bmo1504834-part1
%patch507 -p1 -b .mozilla-bmo1504834-part2
%patch508 -p1 -b .mozilla-bmo1504834-part3
%patch509 -p1 -b .mozilla-bmo1504834-part4
%patch510 -p1 -b .mozilla-bmo1554971
%patch511 -p1 -b .mozilla-bmo1602730
%patch512 -p1 -b .mozilla-bmo849632
%patch513 -p1 -b .mozilla-bmo998749
%patch514 -p1 -b .mozilla-s390x-skia-gradient
%patch515 -p1 -b .mozilla-bmo1626236
%patch516 -p1 -b .D87019-thin-vec-big-endian.diff


%patch1001 -p1 -b .ppc64le-inline
%patch1004 -p1 -b .icu-make
%patch1006 -p1 -b .D89554-autoconf1.diff
%patch1007 -p1 -b .D94538-autoconf2.diff


%{__rm} -f .mozconfig
%{__cp} %{SOURCE10} .mozconfig
function add_to_mozconfig() {
  mozconfig_entry=$1
  echo "ac_add_options --$1" >> .mozconfig
}

# Modify mozconfig file
%if %{official_branding}
 add_to_mozconfig "enable-official-branding"
%endif
%{__cp} %{SOURCE24} mozilla-api-key
%{__cp} %{SOURCE27} google-api-key

%if %{?system_nss}
 add_to_mozconfig "with-system-nspr"
 add_to_mozconfig "with-system-nss"
%else
 add_to_mozconfig "without-system-nspr"
 add_to_mozconfig "without-system-nss"
%endif

%if 0%{?use_bundled_ffi}
 add_to_mozconfig "with-system-ffi"
%endif

%if 0%{?system_ffi}
 add_to_mozconfig "with-system-ffi"
%endif
%ifarch %{arm} %{ix86} x86_64
 add_to_mozconfig "disable-elf-hack"
%endif

%if %{?system_hunspell}
echo "ac_add_options --enable-system-hunspell" >> .mozconfig
%else
# not available?
#echo "ac_add_options --disable-system-hunspell" >> .mozconfig
%endif

%if %{?debug_build}
 add_to_mozconfig "enable-debug"
 add_to_mozconfig "disable-optimize"
%else
%global optimize_flags "-g -O2"
%ifarch s390 s390x
%global optimize_flags "-g -O1"
%endif
%ifarch armv7hl
# ARMv7 need that (rhbz#1426850)
%global optimize_flags "-g -O2 -fno-schedule-insns"
%endif
%ifarch ppc64le aarch64
%global optimize_flags "-g -O2"
%endif
%if %{optimize_flags} != "none"
echo 'ac_add_options --enable-optimize=%{?optimize_flags}' >> .mozconfig
%else
echo 'ac_add_options --enable-optimize' >> .mozconfig
%endif
echo "ac_add_options --disable-debug" >> .mozconfig
%endif

# Second arches fail to start with jemalloc enabled
%ifnarch %{ix86} x86_64
echo "ac_add_options --disable-jemalloc" >> .mozconfig
%endif

%ifnarch %{ix86} x86_64
echo "ac_add_options --disable-webrtc" >> .mozconfig
%endif

%if !%{?system_jpeg}
echo "ac_add_options --without-system-jpeg" >> .mozconfig
%else
echo "ac_add_options --with-system-jpeg" >> .mozconfig
%endif

%if %{?system_libvpx}
echo "ac_add_options --with-system-libvpx" >> .mozconfig
%else
echo "ac_add_options --without-system-libvpx" >> .mozconfig
%endif

%if %{?system_libicu}
echo "ac_add_options --with-system-icu" >> .mozconfig
%else
echo "ac_add_options --without-system-icu" >> .mozconfig
%endif
%ifarch s390 s390x
echo "ac_add_options --disable-jit" >> .mozconfig
%endif

%ifnarch %{ix86}
%if !0%{?debug_build}
echo "ac_add_options --disable-debug-symbols" >> .mozconfig
%endif
%endif

echo 'export NODEJS="%{_buildrootdir}/bin/node-stdout-nonblocking-wrapper"' >> .mozconfig

# Remove executable bit to make brp-mangle-shebangs happy.
chmod -x third_party/rust/itertools/src/lib.rs
chmod a-x third_party/rust/gfx-backend-vulkan/src/*.rs
chmod a-x third_party/rust/gfx-hal/src/*.rs
chmod a-x third_party/rust/ash/src/extensions/ext/*.rs
chmod a-x third_party/rust/ash/src/extensions/khr/*.rs
chmod a-x third_party/rust/ash/src/extensions/mvk/*.rs
chmod a-x third_party/rust/ash/src/extensions/nv/*.rs

# install lightning langpacks

%build
# Disable LTO to work around rhbz#1883904
%define _lto_cflags %{nil}
ulimit -a
free
#set -e

#GTK3 >>
%if ! 0%{?avoid_bundled_rebuild}
    rm -rf %{_buildrootdir}/*
%endif
export PATH="%{_buildrootdir}/bin:$PATH"

function install_rpms_to_current_dir() {
    PACKAGE_RPM=$(eval echo $1)
    PACKAGE_DIR=%{_rpmdir}

    if [ ! -f $PACKAGE_DIR/$PACKAGE_RPM ]; then
        # Hack for tps tests
        ARCH_STR=%{_arch}
        %ifarch i386 i686
            ARCH_STR="i?86"
        %endif
        PACKAGE_DIR="$PACKAGE_DIR/$ARCH_STR"
     fi

     for package in $(ls $PACKAGE_DIR/$PACKAGE_RPM)
     do
         echo "$package"
         rpm2cpio "$package" | cpio -idu
     done
}

function build_bundled_package() {
  PACKAGE_RPM=$1
  PACKAGE_FILES=$2
  PACKAGE_SOURCE=$3
  PACKAGE_BUILD_OPTIONS=$4
  export PACKAGE_DIR="%{_topdir}/RPMS"

  PACKAGE_ALREADY_BUILD=0
  %if %{?avoid_bundled_rebuild}
    if ls $PACKAGE_DIR/$PACKAGE_RPM; then
      PACKAGE_ALREADY_BUILD=1
    fi
    if ls $PACKAGE_DIR/%{_arch}/$PACKAGE_RPM; then
      PACKAGE_ALREADY_BUILD=1
    fi
  %endif
  if [ $PACKAGE_ALREADY_BUILD == 0 ]; then
    echo "Rebuilding $PACKAGE_RPM from $PACKAGE_SOURCE"; echo "==============================="
    rpmbuild --nodeps $PACKAGE_BUILD_OPTIONS --rebuild $PACKAGE_SOURCE
    cat /var/tmp/rpm-tmp*
  fi

  find $PACKAGE_DIR
  if [ ! -f $PACKAGE_DIR/$PACKAGE_RPM ]; then
    # Hack for tps tests
    ARCH_STR=%{_arch}
    %ifarch i386 i686
    ARCH_STR="i?86"
    %endif
    export PACKAGE_DIR="$PACKAGE_DIR/$ARCH_STR"
  fi
  pushd $PACKAGE_DIR

  echo "Installing $PACKAGE_DIR/$PACKAGE_RPM"; echo "==============================="
  pwd
  PACKAGE_LIST=$(echo $PACKAGE_DIR/$PACKAGE_RPM | tr " " "\n")
  for PACKAGE in $PACKAGE_LIST
  do
      rpm2cpio $PACKAGE | cpio -iduv
  done

  PATH=$PACKAGE_DIR/usr/bin:$PATH
  export PATH
  LD_LIBRARY_PATH=$PACKAGE_DIR/usr/%{_lib}:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH

  # Clean rpms to avoid including them to package
  %if ! 0%{?avoid_bundled_rebuild}
    rm -f $PACKAGE_FILES
  %endif

  popd
}

# Build and install local yasm if needed
# ======================================
%if 0%{?use_bundled_yasm}
  build_bundled_package 'yasm-1*.rpm' 'yasm-*.rpm' '%{SOURCE301}'
%endif

%if 0%{?bundle_nss}
   rpm -ivh %{SOURCE402}
   #rpmbuild --nodeps --define '_prefix %{gtk3_install_path}' --without=tests -ba %{_specdir}/nspr.spec
   rpmbuild --nodeps --define '_prefix %{gtk3_install_path}' -ba %{_specdir}/nspr.spec
   pushd %{_buildrootdir}
   install_rpms_to_current_dir nspr-4*.rpm
   install_rpms_to_current_dir nspr-devel*.rpm
   popd
   echo "Setting nspr flags"
   # nss-setup-flags-env.inc
   sed -i 's@%{gtk3_install_path}@%{_buildrootdir}%{gtk3_install_path}@g' %{_buildrootdir}%{gtk3_install_path}/%{_lib}/pkgconfig/nspr*.pc

   export LDFLAGS="-L%{_buildrootdir}%{gtk3_install_path}/%{_lib} $LDFLAGS"
   export LDFLAGS="-Wl,-rpath,%{gtk3_install_path}/%{_lib} $LDFLAGS"
   export LDFLAGS="-Wl,-rpath-link,%{_buildrootdir}%{gtk3_install_path}/%{_lib} $LDFLAGS"
   export PKG_CONFIG_PATH=%{_buildrootdir}%{gtk3_install_path}/%{_lib}/pkgconfig
   export PATH="{_buildrootdir}%{gtk3_install_path}/bin:$PATH"

   export PATH=%{_buildrootdir}/%{gtk3_install_path}/bin:$PATH
   echo $PKG_CONFIG_PATH

   rpm -ivh %{SOURCE403}
   rpmbuild --nodeps --define '_prefix %{gtk3_install_path}' -ba %{_specdir}/nss.spec
   pushd %{_buildrootdir}
   #cleanup
   #rm -rf {_buildrootdir}/usr/lib/debug/*
   #rm -rf {_buildrootdir}/usr/lib/.build-id
   #install_rpms_to_current_dir nss-%{gtk3_nvr}*.rpm
   #install_rpms_to_current_dir nss-devel-%{gtk3_nvr}*.rpm
   install_rpms_to_current_dir nss-3*.rpm
   install_rpms_to_current_dir nss-devel*.rpm
   install_rpms_to_current_dir nss-pkcs11-devel*.rpm
   install_rpms_to_current_dir nss-softokn-3*.rpm
   install_rpms_to_current_dir nss-softokn-devel*.rpm
   install_rpms_to_current_dir nss-softokn-freebl-3*.rpm
   install_rpms_to_current_dir nss-softokn-freebl-devel*.rpm
   install_rpms_to_current_dir nss-util-3*.rpm
   install_rpms_to_current_dir nss-util-devel*.rpm
   popd
  %filter_provides_in %{gtk3_install_path}/%{_lib}
  %filter_requires_in %{gtk3_install_path}/%{_lib}
  %filter_from_requires /libnss3.so.*/d
  %filter_from_requires /libsmime3.so.*/d
  %filter_from_requires /libssl3.so.*/d
  %filter_from_requires /libnssutil3.so.*/d
  %filter_from_requires /libnspr4.so.*/d
%endif

%if 0%{use_bundled_ffi}
  # Install libraries to the predefined location to later add them to the Firefox libraries
  rpm -ivh %{SOURCE303}
  rpmbuild --nodeps --define '_prefix %{gtk3_install_path}' -ba %{_specdir}/libffi.spec
  pushd %{_buildrootdir}
  install_rpms_to_current_dir 'libffi*.rpm'
  popd
  %filter_from_requires /libffi.so.6/d
%endif
%filter_setup

# If needed build the bundled python 2.7 and 3.6 and put it in the PATH
%if 0%{?use_bundled_python_3}
    pushd %{_builddir}/python3/Python-%{bundled_python_version_3}
    ./configure --prefix="%{_buildrootdir}" --exec-prefix="%{_buildrootdir}" --libdir="%{_buildrootdir}/lib" || cat config.log
    make %{?_smp_mflags} install V=1 -j1
    cp Tools/scripts/pathfix.py %{_buildrootdir}/bin
    popd
%endif
%if 0%{?use_bundled_python_2}
    pushd %{_builddir}/python2/Python-%{bundled_python_version_2}
    ./configure --prefix="%{_buildrootdir}" --exec-prefix="%{_buildrootdir}" --libdir="%{_buildrootdir}/lib" || cat config.log
    make %{?_smp_mflags} install V=1
    popd    
%endif

function replace_prefix() {
  FILE_NAME=$1
  PKG_CONFIG_PREFIX=$2

  cat $FILE_NAME | tail -n +2 > tmp.txt
  echo "$PKG_CONFIG_PREFIX" > $FILE_NAME
  cat tmp.txt >> $FILE_NAME
  rm -rf tmp.txt
}

# Build and install local openssl if needed
# =========================================
%if 0%{?use_bundled_openssl}
  rpm -ivh %{SOURCE305}
  rpmbuild --nodeps -ba %{_specdir}/openssl.spec
  pushd %{_buildrootdir}
  install_rpms_to_current_dir openssl-1.0.2k*.rpm
  install_rpms_to_current_dir openssl-libs-1.0.2k*.rpm
  install_rpms_to_current_dir openssl-devel-1.0.2k*.rpm
  install_rpms_to_current_dir openssl-static-1.0.2k*.rpm
  # openssl is installed to %{_buildrootdir}/usr/lib(64)/...
  export PKG_CONFIG_PATH=%{_buildrootdir}/%{_libdir}/pkgconfig/:$PKG_CONFIG_PATH
  replace_prefix %{_buildrootdir}/%{_libdir}/pkgconfig/libcrypto.pc prefix=%{_buildrootdir}/usr
  replace_prefix %{_buildrootdir}/%{_libdir}/pkgconfig/libssl.pc prefix=%{_buildrootdir}/usr
  replace_prefix %{_buildrootdir}/%{_libdir}/pkgconfig/openssl.pc prefix=%{_buildrootdir}/usr
  cat  %{_buildrootdir}/%{_libdir}/pkgconfig/libcrypto.pc
  cat  %{_buildrootdir}/%{_libdir}/pkgconfig/libssl.pc
  cat  %{_buildrootdir}/%{_libdir}/pkgconfig/openssl.pc
  pushd %{_rpmdir}
  rm -f openssl-*.rpm
  popd
  popd
%endif

# GTK3 <<
# We need to disable exit on error temporarily for the following scripts:
set +e
%if 0%{?use_dts}
source scl_source enable devtoolset-%{dts_version}
%endif
%if 0%{?use_rustts}
source scl_source enable rust-toolset-%{rust_toolset_version}
%endif

env
which gcc
which c++
which g++
which ld
# Build and install local node if needed
# ======================================
%if %{use_bundled_nodejs}
  build_bundled_package 'nodejs-10*.rpm' 'nodejs-*.rpm npm-*.rpm' %{SOURCE304} "--with bootstrap"
  export MOZ_NODEJS=$PACKAGE_DIR/usr/bin/node
%else
  export MOZ_NODEJS=/usr/bin/node
%endif

mkdir -p my_rust_vendor
cd my_rust_vendor
%{__tar} xf %{SOURCE2}
cd -
mkdir -p .cargo
cat > .cargo/config <<EOL
[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "`pwd`/my_rust_vendor"
EOL

export CARGO_HOME=.cargo
cargo install cbindgen
export PATH=`pwd`/.cargo/bin:$PATH
export CBINDGEN=`pwd`/.cargo/bin/cbindgen


# debug missing sqlite3 python module
./mach python -c "import sys;print(sys.path)"

%if 0%{?big_endian}
  echo "Generate big endian version of config/external/icu/data/icud58l.dat"
  icupkg -tb config/external/icu/data/icudt67l.dat config/external/icu/data/icudt67b.dat
  ls -l config/external/icu/data
  rm -f config/external/icu/data/icudt*l.dat
%endif

mkdir %{_buildrootdir}/bin || :
cp %{SOURCE28} %{_buildrootdir}/bin || :
chmod +x %{_buildrootdir}/bin/node-stdout-nonblocking-wrapper

# Update the various config.guess to upstream release for aarch64 support
find ./ -name config.guess -exec cp /usr/lib/rpm/config.guess {} ';'

# -fpermissive is needed to build with gcc 4.6+ which has become stricter
#
# Mozilla builds with -Wall with exception of a few warnings which show up
# everywhere in the code; so, don't override that.
#
# Disable C++ exceptions since Mozilla code is not exception-safe
#
MOZ_OPT_FLAGS=$(echo "%{optflags}" | %{__sed} -e 's/-Wall//')
#rhbz#1037063
# -Werror=format-security causes build failures when -Wno-format is explicitly given
# for some sources
# Explicitly force the hardening flags for Firefox so it passes the checksec test;
# See also https://fedoraproject.org/wiki/Changes/Harden_All_Packages
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -Wformat-security -Wformat -Werror=format-security"
%if 0%{?fedora} > 23
# Disable null pointer gcc6 optimization in gcc6 (rhbz#1328045)
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -fno-delete-null-pointer-checks"
%endif
# Use hardened build?
%if %{?hardened_build}
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -fPIC -Wl,-z,relro -Wl,-z,now"
%endif
%if %{?debug_build}
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | %{__sed} -e 's/-O2//')
%endif
%ifarch s390
MOZ_OPT_FLAGS=$(echo "$MOZ_OPT_FLAGS" | %{__sed} -e 's/-g/-g1/')
# If MOZ_DEBUG_FLAGS is empty, firefox's build will default it to "-g" which
# overrides the -g1 from line above and breaks building on s390
# (OOM when linking, rhbz#1238225)
export MOZ_DEBUG_FLAGS=" "
%endif

# We don't wantfirefox to use CK_GCM_PARAMS_V3 in nss
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -DNSS_PKCS11_3_0_STRICT"

%ifarch s390 %{arm} ppc aarch64 i686 x86_64 s390x
MOZ_LINK_FLAGS="-Wl,--no-keep-memory -Wl,--reduce-memory-overheads"
%endif
%ifarch %{arm} i686
MOZ_LINK_FLAGS="-Wl,--no-keep-memory -Wl,--strip-debug"
echo "ac_add_options --enable-linker=gold" >> .mozconfig
%endif

%ifarch %{arm} i686
export RUSTFLAGS="-Cdebuginfo=0"
%endif
export CFLAGS=$MOZ_OPT_FLAGS
export CXXFLAGS=$MOZ_OPT_FLAGS
export LDFLAGS=$MOZ_LINK_FLAGS

export PREFIX='%{_prefix}'
export LIBDIR='%{_libdir}'
export CC=gcc
export CXX=g++

MOZ_SMP_FLAGS=-j1
# More than two build tasks can lead to OOM gcc crash.
%if 0%{?rhel} < 8
[ -z "$RPM_BUILD_NCPUS" ] && \
     RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
[ "$RPM_BUILD_NCPUS" -ge 2 ] && MOZ_SMP_FLAGS=-j2
%else
%ifarch %{ix86} x86_64 ppc ppc64 ppc64le aarch64
[ -z "$RPM_BUILD_NCPUS" ] && \
     RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
[ "$RPM_BUILD_NCPUS" -ge 2 ] && MOZ_SMP_FLAGS=-j2
[ "$RPM_BUILD_NCPUS" -ge 4 ] && MOZ_SMP_FLAGS=-j3
[ "$RPM_BUILD_NCPUS" -ge 8 ] && MOZ_SMP_FLAGS=-j3
%endif
%endif

cat /proc/meminfo
# Free memory in kB

# Free memory in kB
if grep -q MemAvailable /proc/meminfo; then
    MEM_AVAILABLE=$(grep MemAvailable /proc/meminfo | awk '{ print $2 }')
else
    MEM_AVAILABLE=$(grep MemFree /proc/meminfo | awk '{ print $2 }')
fi

# Usually the compiler processes can take 2 GB of memory at peaks
TASK_SIZE=4000000
MEM_CONSTRAINED_JOBS=$(( MEM_AVAILABLE / TASK_SIZE ))

if [ $MEM_CONSTRAINED_JOBS -le 0 ]; then
  MEM_CONSTRAINED_JOBS=1
fi

CPU_AVAILABLE=$(/usr/bin/getconf _NPROCESSORS_ONLN)
# Pick the minimum from available CPUs or memory constrained number of jobs
MOZ_SMP_FLAGS=-j$([ "$CPU_AVAILABLE" -le "$MEM_CONSTRAINED_JOBS" ] && echo "$CPU_AVAILABLE" || echo "$MEM_CONSTRAINED_JOBS")

%if 0%{?bundle_nss}
echo "Setting nss flags"
# nss-setup-flags-env.inc
%include_file %{SOURCE401}
export PATH=%{_buildrootdir}/%{gtk3_install_path}/bin:$PATH
echo $PKG_CONFIG_PATH
%endif

export MOZ_MAKE_FLAGS="$MOZ_SMP_FLAGS"
#export MOZ_SERVICES_SYNC="1"
# we need to strip the sources on i686 because to we don't use rpm to generate debugsymbols because of oom
%ifnarch i686 i386
export STRIP=/bin/true
%endif
which node
echo 'export NODEJS="%{_buildrootdir}/bin/node-stdout-nonblocking-wrapper"'
env
ls %{_buildrootdir}


%if 0%{?use_llvmts}
scl enable llvm-toolset-%{llvm_version} './mach build -v'
%else
./mach build -v
%endif

#---------------------------------------------------------------------

%install

function install_rpms_to_current_dir() {
    PACKAGE_RPM=$(eval echo $1)
    PACKAGE_DIR=%{_rpmdir}

    if [ ! -f $PACKAGE_DIR/$PACKAGE_RPM ]; then
        # Hack for tps tests
        ARCH_STR=%{_arch}
        %ifarch i386 i686
            ARCH_STR="i?86"
        %endif
        PACKAGE_DIR="$PACKAGE_DIR/$ARCH_STR"
     fi

     for package in $(ls $PACKAGE_DIR/$PACKAGE_RPM)
     do
         echo "$package"
         rpm2cpio "$package" | cpio -idu
     done
}

%if 0%{?bundle_nss}
  pushd %{buildroot}
  #install_rpms_to_current_dir nss-*.rpm
  install_rpms_to_current_dir nspr-4*.rpm
  install_rpms_to_current_dir nss-3*.rpm
  install_rpms_to_current_dir nss-softokn-3*.rpm
  install_rpms_to_current_dir nss-softokn-freebl-3*.rpm
  install_rpms_to_current_dir nss-util-3*.rpm
 
  # cleanup unecessary nss files
  #rm -rf %{_buildrootdir}/%{gtk3_install_path}/bin
  #rm -rf %{_buildrootdir}/%{gtk3_install_path}/include
  rm -rf %{buildroot}/%{gtk3_install_path}/lib/dracut
  rm -rf %{buildroot}/%{gtk3_install_path}/%{_lib}/nss
  #rm -rf %{_buildrootdir}/%{gtk3_install_path}/%{_lib}/pkgconfig
  rm -rf %{buildroot}/%{gtk3_install_path}/%{_lib}/share
  rm -rf %{buildroot}/%{gtk3_install_path}/share
  rm -rf %{buildroot}/etc/pki
  rm -rf %{buildroot}/usr/lib/.build-id
  rm -rf %{buildroot}/etc/crypto-policies
  popd
%endif

# Install bundled libffi
%if %{use_bundled_ffi}
  pushd %{buildroot}
  install_rpms_to_current_dir libffi-3*.rpm
  popd
%endif

DESTDIR=%{buildroot} make -C objdir install

%{__mkdir_p} %{buildroot}{%{_libdir},%{_bindir},%{_datadir}/applications}

desktop-file-install --dir %{buildroot}%{_datadir}/applications %{SOURCE20}

# set up the thunderbird start script
rm -rf %{buildroot}%{_bindir}/thunderbird
%{__rm} -rf %{buildroot}%{_bindir}/thunderbird
%{__cat} %{SOURCE21} > %{buildroot}%{_bindir}/thunderbird
sed -i -e 's|%RHEL_ENV_VARS%||' %{buildroot}%{_bindir}/thunderbird
%{__chmod} 755 %{buildroot}%{_bindir}/thunderbird

# Setup preferences, depends on RHEL version
THUNDERBIRD_PREF_SOURCE=%{SOURCE701}
%if 0%{?rhel} == 6
  THUNDERBIRD_PREF_SOURCE=%{SOURCE601}
%endif

# Fill in THUNDERBIRD_RPM_VR into our rh-default-prefs
%{__cat} $THUNDERBIRD_PREF_SOURCE | %{__sed} -e 's,THUNDERBIRD_RPM_VR,%{version}-%{release},g' > \
        %{buildroot}/rh-default-prefs
%{__install} -D %{buildroot}/rh-default-prefs %{buildroot}/%{mozappdir}/greprefs/all-redhat.js
%{__install} -D %{buildroot}/rh-default-prefs %{buildroot}/%{mozappdir}/defaults/pref/all-redhat.js
%{__rm} %{buildroot}/rh-default-prefs

# install icons
for s in 16 22 24 32 48 256; do
    %{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps
    %{__cp} -p comm/mail/branding/%{name}/default${s}.png \
               %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps/thunderbird.png
done

%{__rm} -f %{buildroot}%{_bindir}/thunderbird-config

# own mozilla plugin dir (#135050)
%{__mkdir_p} %{buildroot}%{_libdir}/mozilla/plugins

# own extension directories
%{__mkdir_p} %{buildroot}%{_datadir}/mozilla/extensions/%{thunderbird_app_id}
%{__mkdir_p} %{buildroot}%{_libdir}/mozilla/extensions/%{thunderbird_app_id}

# Install langpacks
echo > %{name}.lang
%if %{build_langpacks}
# Extract langpacks, make any mods needed, repack the langpack, and install it.
%{__mkdir_p} %{buildroot}%{langpackdir}
%{__tar} xf %{SOURCE1}
for langpack in `ls thunderbird-langpacks/*.xpi`; do
  language=`basename $langpack .xpi`
  extensionID=langpack-$language@thunderbird.mozilla.org
  %{__mkdir_p} $extensionID
  unzip $langpack -d $extensionID
  find $extensionID -type f | xargs chmod 644

  cd $extensionID
  zip -r9mX ../${extensionID}.xpi *
  cd -

  %{__install} -m 644 ${extensionID}.xpi %{buildroot}%{langpackdir}
  language=`echo $language | sed -e 's/-/_/g'`
  echo "%%lang($language) %{langpackdir}/${extensionID}.xpi" >> %{name}.lang
done
%{__rm} -rf thunderbird-langpacks

echo "Adding following langpacks:"
cat %{name}.lang
%endif

# Install feature extensions.
echo > %{name}.features
if [ -f "$(ls -1 %{mozappfeatures} | head -1)" ]; then
    ls -1 %{mozappfeatures} >> %{name}.features
    echo "Adding following features:"
    cat %{name}.features
fi

# Get rid of devel package and its debugsymbols
%{__rm} -rf %{buildroot}%{_libdir}/%{name}-devel-%{version}

# Copy over the LICENSE
%{__install} -p -c -m 644 LICENSE %{buildroot}/%{mozappdir}

# Use the system hunspell dictionaries
%{__rm} -rf %{buildroot}%{mozappdir}/dictionaries
ln -s %{_datadir}/myspell %{buildroot}%{mozappdir}/dictionaries

# ghost files
%{__mkdir_p} %{buildroot}%{mozappdir}/components
touch %{buildroot}%{mozappdir}/components/compreg.dat
touch %{buildroot}%{mozappdir}/components/xpti.dat

# Clean thunderbird-devel debuginfo
rm -rf %{_prefix}/lib/debug/lib/%{name}-devel-*
rm -rf %{_prefix}/lib/debug/lib64/%{name}-devel-*

# Fixing python version
test -f "%{buildroot}%{mozappdir}/distribution/extensions/unicode-segmentation/scripts/unicode_gen_breaktests.py" && sed -i -e 's|/usr/bin/env python$|/usr/bin/env python2|' %{buildroot}%{mozappdir}/distribution/extensions/unicode-segmentation/scripts/unicode_gen_breaktests.py
test -f "%{buildroot}%{mozappdir}/distribution/extensions/unicode-segmentation/scripts/unicode.py" && sed -i -e 's|/usr/bin/env python$|/usr/bin/env python2|' %{buildroot}%{mozappdir}/distribution/extensions/unicode-segmentation/scripts/unicode.py
test -f "%{buildroot}%{mozappdir}/distribution/extensions/unicode-width/scripts/unicode.py" && sed -i -e 's|/usr/bin/env python$|/usr/bin/env python2|' %{buildroot}%{mozappdir}/distribution/extensions/unicode-width/scripts/unicode.py

# Removing librnp.so - we cannot deliver that in RHELs
%{__rm} -rf %{buildroot}%{mozappdir}/librnp.so

#---------------------------------------------------------------------

%clean
rm -rf %{_srcrpmdir}/gtk3-private-%{gtk3_nvr}*.src.rpm
find %{_rpmdir} -name "gtk3-private-*%{gtk3_nvr}*.rpm" -delete
rm -rf %{_srcrpmdir}/libffi*.src.rpm
find %{_rpmdir} -name "libffi*.rpm" -delete
rm -rf %{_srcrpmdir}/openssl*.src.rpm
find %{_rpmdir} -name "openssl*.rpm" -delete
rm -rf %{_srcrpmdir}/nss*.src.rpm
find %{_rpmdir} -name "nss*.rpm" -delete
rm -rf %{_srcrpmdir}/nspr*.src.rpm
find %{_rpmdir} -name "nspr*.rpm" -delete

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
update-desktop-database &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

#===============================================================================
%files -f %{name}.lang -f %{name}.features
%defattr(-,root,root,-)
%attr(755,root,root) %{_bindir}/thunderbird
%attr(644,root,root) %{_datadir}/applications/thunderbird.desktop
%dir %{_datadir}/mozilla/extensions/%{thunderbird_app_id}
%dir %{_libdir}/mozilla/extensions/%{thunderbird_app_id}
%dir %{mozappdir}
%doc %{mozappdir}/LICENSE
%{mozappdir}/chrome
%dir %{mozappdir}/components
%ghost %{mozappdir}/components/compreg.dat
%ghost %{mozappdir}/components/xpti.dat
%{mozappdir}/omni.ja
%{mozappdir}/plugin-container
%{mozappdir}/defaults
%{mozappdir}/dictionaries
%{mozappdir}/greprefs
%{mozappdir}/isp
%{mozappdir}/thunderbird-bin
%{mozappdir}/thunderbird
%{mozappdir}/*.so
%{mozappdir}/platform.ini
%{mozappdir}/application.ini
%exclude %{mozappdir}/removed-files
%{_datadir}/icons/hicolor/16x16/apps/thunderbird.png
%{_datadir}/icons/hicolor/22x22/apps/thunderbird.png
%{_datadir}/icons/hicolor/24x24/apps/thunderbird.png
%{_datadir}/icons/hicolor/256x256/apps/thunderbird.png
%{_datadir}/icons/hicolor/32x32/apps/thunderbird.png
%{_datadir}/icons/hicolor/48x48/apps/thunderbird.png
%{mozappdir}/pingsender
%{mozappdir}/gtk2/libmozgtk.so
%{mozappdir}/dependentlibs.list
%dir %{mozappdir}/distribution
%{mozappdir}/fonts/TwemojiMozilla.ttf

%if !%{?system_libicu}
#%%{mozappdir}/icudt*.dat
%endif
%if !%{?system_nss}
%exclude %{mozappdir}/libnssckbi.so
%endif
%if 0%{use_bundled_ffi}
%{mozappdir}/bundled/%{_lib}/libffi.so*
%exclude %{_datadir}/doc/libffi*
%endif

%if 0%{?bundle_nss}
%{mozappdir}/bundled/%{_lib}/libfreebl*
%{mozappdir}/bundled/%{_lib}/libnss3*
%{mozappdir}/bundled/%{_lib}/libnssdbm3*
%{mozappdir}/bundled/%{_lib}/libnssutil3*
%{mozappdir}/bundled/%{_lib}/libsmime3*
%{mozappdir}/bundled/%{_lib}/libsoftokn*
%{mozappdir}/bundled/%{_lib}/libssl3*
%{mozappdir}/bundled/%{_lib}/libnspr4.so
%{mozappdir}/bundled/%{_lib}/libplc4.so
%{mozappdir}/bundled/%{_lib}/libplds4.so
%endif

#===============================================================================

%changelog
* Tue Aug 10 2021 Eike Rathke <erack@redhat.com> - 78.13.0-1
- Update to 78.13.0

* Tue Aug 10 2021 Mohan Boddu <mboddu@redhat.com> - 78.12.0-4
- Rebuilt for IMA sigs, glibc 2.34, aarch64 flags
  Related: rhbz#1991688

* Fri Jul 30 2021 Tomas Popela <tpopela@redhat.com> - 78.12.0-3
- Add script to process the official tarball to comply with PELC review
- Fix the build with newer glibc

* Mon Jul 12 2021 Eike Rathke <erack@redhat.com> - 78.12.0-2
- Update to 78.12.0 build2

* Thu Jul 08 2021 Eike Rathke <erack@redhat.com> - 78.12.0-1
- Update to 78.12.0 build1

* Wed Jun 30 2021 Jan Horak <jhorak@redhat.com> - 78.11.0-2
- Added bundled libraries, update to 78.11

* Tue Jun 22 2021 Mohan Boddu <mboddu@redhat.com> - 78.8.0-5
- Rebuilt for RHEL 9 BETA for openssl 3.0
  Related: rhbz#1971065

* Fri Apr 16 2021 Mohan Boddu <mboddu@redhat.com> - 78.8.0-4
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937
- Fixing MOZ_SMP_FLAGS

* Mon Mar 01 2021 Jan Horak <jhorak@redhat.com> - 78.8.0-2
- Removed autoconf213 dependency

* Mon Feb 22 2021 Jan Horak <jhorak@redhat.com> - 78.8.0-1
- Update to 78.8.0 build1

* Tue Jan 12 2021 Eike Rathke <erack@redhat.com> - 78.6.1-1
- Update to 78.6.1

* Tue Dec 15 2020 Eike Rathke <erack@redhat.com> - 78.6.0-1
- Update to 78.6.0

* Fri Dec 04 2020 Jan Horak <jhorak@redhat.com> - 78.5.1-1
- Update to 78.5.1 build1

* Wed Nov 18 2020 Eike Rathke <erack@redhat.com> - 78.5.0-1
- Update to 78.5.0 build3

* Thu Nov 12 2020 Eike Rathke <erack@redhat.com> - 78.4.3-1
- Update to 78.4.3

* Wed Oct 21 2020 Eike Rathke <erack@redhat.com> - 78.4.0-1
- Update to 78.4.0 build1
- Disabled telemetry

* Tue Sep 29 2020 Jan Horak <jhorak@redhat.com> - 78.3.1-1
- Update to 78.3.1 build1

* Sat Sep 19 2020 Jan Horak <jhorak@redhat.com> - 78.3.0-3
- Update to 78.3.0 build1
- Remove librdp.so as long as we cannot ship it in RHEL

* Tue Sep 08 2020 Jan Horak <jhorak@redhat.com> - 78.2.1-1
- Update to 78.2.1 build1

* Wed Sep 02 2020 Jan Horak <jhorak@redhat.com> - 68.12.0-1
- Update to 68.12.0 build1

* Tue Aug 04 2020 Jan Horak <jhorak@redhat.com> - 68.11.0-1
- Update to 68.11.0 build1

* Wed Jul 08 2020 Jan Horak <jhorak@redhat.com> - 68.10.0-1
- Update to 68.10.0 build1

* Fri Jun 05 2020 Jan Horak <jhorak@redhat.com> - 68.9.0-1
- Update to 68.9.0 build1

* Tue May 05 2020 Jan Horak <jhorak@redhat.com> - 68.8.0-1
- Update to 68.8.0 build2

* Tue Apr 14 2020 Jan Horak <jhorak@redhat.com> - 68.7.0-1
- Update to 68.7.0 build1

* Fri Mar 13 2020 Jan Horak <jhorak@redhat.com> - 68.6.0-1
- Update to 68.6.0 build2

* Thu Feb 13 2020 Jan Horak <jhorak@redhat.com> - 68.5.0-1
- Update to 68.5.0 build1

* Mon Jan 13 2020 Jan Horak <jhorak@redhat.com> - 68.4.1-2
- Update to 68.4.1 build1

* Mon Dec 02 2019 Jan Horak <jhorak@redhat.com> - 68.3.0-2
- Update to 68.3.0 build2

* Fri Oct 25 2019 Jan Horak <jhorak@redhat.com> - 68.2.0-2
- Added patch for TLS 1.3 support.

* Tue Oct 22 2019 Jan Horak <jhorak@redhat.com> - 68.2.0-1
- Update to 68.2.0

* Thu Oct  3 2019 Jan Horak <jhorak@redhat.com> - 68.1.1-2
- Update to 68.1.1

* Wed Sep  4 2019 Jan Horak <jhorak@redhat.com> - 60.9.0-2
- Update to 60.9.0

* Thu Jul 4 2019 Martin Stransky <stransky@redhat.com> - 60.8.0-1
- Updated to 60.8.0

* Wed Jul 3 2019 Martin Stransky <stransky@redhat.com> - 60.7.2-3
- Rebuild to fix rhbz#1725919 - Thunderbird fails to authenticate
  with gmail with ssl/tls and OAuth2.

* Fri Jun 21 2019 Jan Horak <jhorak@redhat.com> - 60.7.2-2
- Update to 60.7.2 build2

* Thu Jun 20 2019 Jan Horak <jhorak@redhat.com> - 60.7.2-1
- Update to 60.7.2

* Tue Jun 18 2019 Jan Horak <jhorak@redhat.com> - 60.7.1-1
- Update to 60.7.1

* Mon May 27 2019 Martin Stransky <stransky@redhat.com> - 60.7.0-1
- Update to 60.7.0

* Mon Mar 25 2019 Martin Stransky <stransky@redhat.com> - 60.6.1-1
- Update to 60.6.1

* Tue Mar 19 2019 Martin Stransky <stransky@redhat.com> - 60.6.0-1
- Update to 60.6.0

* Tue Jan 29 2019 Martin Stransky <stransky@redhat.com> - 60.5.0-1
- Update to 60.5.0

* Thu Jan  3 2019 Jan Horak <jhorak@redhat.com> - 60.4.0-1
- Update to 60.4.0

* Wed Oct 31 2018 Jan Horak <jhorak@redhat.com> - 60.3.0-1
- Update to 60.3.0

* Wed Oct 31 2018 Jan Horak <jhorak@redhat.com> - 60.2.1-6
- Fixed missing calendar langpacks

* Tue Oct 16 2018 Jan Horak <jhorak@redhat.com> - 60.2.1-5
- Fixing minor issues

* Wed Oct 10 2018 Jan Horak <jhorak@redhat.com> - 60.2.1-3
- Reverting deleting of key3db

* Wed Oct  3 2018 Jan Horak <jhorak@redhat.com> - 60.2.1-2
- Update to 60.2.1
- Added fix for rhbz#1546988

* Fri Sep 14 2018 Jan Horak <jhorak@redhat.com> - 60.0-1
- Rebase to version 60

* Tue Jul 10 2018 Jan Horak <jhorak@redhat.com> - 52.9.1-1
- Update to 52.9.1

* Thu May 17 2018 Jan Horak <jhorak@redhat.com> - 52.8.0-2
- Update to 52.8.0

* Mon Mar 26 2018 Jan Horak <jhorak@redhat.com> - 52.7.0-1
- Update to 52.7.0

* Fri Jan 26 2018 Jan Horak <jhorak@redhat.com> - 52.6.0-1
- Update to 52.6.0

* Tue Jan  2 2018 Jan Horak <jhorak@redhat.com> - 52.5.2-1
- Update to 52.5.2

* Mon Nov 27 2017 Jan Horak <jhorak@redhat.com> - 52.5.0-1
- Update to 52.5.0

* Wed Oct  4 2017 Jan Horak <jhorak@redhat.com> - 52.4.0-2
- Update to 52.4.0 (b2)

* Mon Aug 21 2017 Jan Horak <jhorak@redhat.com> - 52.3.0-1
- Update to 52.3.0

* Thu Jun 29 2017 Jan Horak <jhorak@redhat.com> - 52.2.1-1
- Update to 52.2.1

* Thu Jun 15 2017 Jan Horak <jhorak@redhat.com> - 52.2.0-1
- Update to 52.2.0

* Tue May  2 2017 Jan Horak <jhorak@redhat.com> - 52.1.0-1
- Update to 52.1.0

* Thu Apr 13 2017 Jan Horak <jhorak@redhat.com> - 52.0.1-1
- Update to 52.0.1

* Tue Mar  7 2017 Jan Horak <jhorak@redhat.com> - 45.8.0-1
- Update to 45.8.0

* Thu Jan 26 2017 Jan Horak <jhorak@redhat.com> - 45.7.0-1
- Update to 45.7.0

* Fri Dec 16 2016 Martin Stransky <stransky@redhat.com> - 45.6.0-1
- Update to the latest upstream (45.6.0)

* Thu Dec  1 2016 Jan Horak <jhorak@redhat.com> - 45.5.1-1
- Update to 45.5.1

* Fri Nov 18 2016 Jan Horak <jhorak@redhat.com> - 45.5.0-1
- Update to 45.5.0

* Thu Sep 29 2016 Jan Horak <jhorak@redhat.com> - 45.4.0-1
- Update to 45.4.0

* Fri Aug 26 2016 Jan Horak <jhorak@redhat.com> - 45.3.0-1
- Update to 45.3.0

* Wed Jun 29 2016 Jan Horak <jhorak@redhat.com> - 45.2-1
- Update to 45.2

* Mon Jun  6 2016 Jan Horak <jhorak@redhat.com> - 45.1.1-1
- Update to 45.1.1

* Mon Jun 06 2016 Jan Horak <jhorak@redhat.com> - 45.1.0-5
- Do not add symlinks to some langpacks

* Tue May 17 2016 Jan Horak <jhorak@redhat.com> - 45.1.0-4
- Update to 45.1.0

* Tue Apr 26 2016 Jan Horak <jhorak@redhat.com> - 45.0-5
- Update to 45.0

* Tue Sep 29 2015 Jan Horak <jhorak@redhat.com> - 38.3.0-1
- Update to 38.3.0

* Fri Aug 14 2015 Jan Horak <jhorak@redhat.com> - 38.2.0-1
- Update to 38.2.0

* Wed Jul 15 2015 Jan Horak <jhorak@redhat.com> - 38.1.0-2
- Rebase to 38.1.0

* Wed Jul 15 2015 Jan Horak <jhorak@redhat.com> - 31.8.0-1
- Update to 31.8.0

* Sun May 10 2015 Jan Horak <jhorak@redhat.com> - 31.7.0-1
- Update to 31.7.0

* Tue Mar 31 2015 Jan Horak <jhorak@redhat.com> - 31.6.0-1
- Update to 31.6.0

* Mon Feb 23 2015 Jan Horak <jhorak@redhat.com> - 31.5.0-2
- Update to 31.5.0

* Sat Jan 10 2015 Jan Horak <jhorak@redhat.com> - 31.4.0-1
- Update to 31.4.0

* Mon Dec 22 2014 Jan Horak <jhorak@redhat.com> - 31.3.0-2
- Fixed problems with dictionaries (mozbz#1097550)

* Fri Nov 28 2014 Jan Horak <jhorak@redhat.com> - 31.3.0-1
- Update to 31.3.0

* Thu Oct 30 2014 Jan Horak <jhorak@redhat.com> - 31.2.0-2
- Update to 31.2.0

* Wed Oct 1 2014 Martin Stransky <stransky@redhat.com> - 31.1.1-5
- Sync preferences with Firefox

* Thu Sep 18 2014 Yaakov Selkowitz <yselkowi@redhat.com> - 31.1.1-4
- Fix dependency generation for internal libraries (#1140471)

* Fri Sep 12 2014 Jan Horak <jhorak@redhat.com> - 31.1.1-3
- Update to 31.1.1

* Tue Sep  9 2014 Jan Horak <jhorak@redhat.com> - 31.1.0-4
- Use  system libffi

* Wed Sep  3 2014 Jan Horak <jhorak@redhat.com> - 31.1.0-2
- Added fix for ppc64le

* Mon Sep  1 2014 Jan Horak <jhorak@redhat.com> - 31.1.0-1
- Update to 31.1.0

* Wed Jul 30 2014 Martin Stransky <stransky@redhat.com> - 31.0-2
- Added patch for mozbz#858919

* Tue Jul 29 2014 Martin Stransky <stransky@redhat.com> - 31.0-1
- Update to 31.0

* Tue Jul 22 2014 Jan Horak <jhorak@redhat.com> - 24.7.0-1
- Update to 24.7.0

* Mon Jun  9 2014 Jan Horak <jhorak@redhat.com> - 24.6.0-1
- Update to 24.6.0

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 24.5.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Fri May 23 2014 Brent Baude <baude@us.ibm.com> - 24.5.0-5
- Moving the ppc64 conditional up before the cd so it will
- apply cleanly

* Fri May 23 2014 Martin Stransky <stransky@redhat.com> - 24.5.0-4
- Added a build fix for ppc64 - rhbz#1100495

* Mon May  5 2014 Jan Horak <jhorak@redhat.com> - 24.5.0-3
- Fixed find requires

* Mon Apr 28 2014 Jan Horak <jhorak@redhat.com> - 24.5.0-1
- Update to 24.5.0

* Tue Apr 22 2014 Jan Horak <jhorak@redhat.com> - 24.4.0-2
- Added support for ppc64le

* Tue Mar 18 2014 Jan Horak <jhorak@redhat.com> - 24.4.0-1
- Update to 24.4.0

* Mon Feb  3 2014 Jan Horak <jhorak@redhat.com> - 24.3.0-1
- Update to 24.3.0

* Mon Dec 16 2013 Martin Stransky <stransky@redhat.com> - 24.2.0-4
- Fixed rhbz#1024232 - thunderbird: squiggly lines used
  for spelling correction disappear randomly

* Fri Dec 13 2013 Martin Stransky <stransky@redhat.com> - 24.2.0-3
- Build with -Werror=format-security (rhbz#1037353)

* Wed Dec 11 2013 Martin Stransky <stransky@redhat.com> - 24.2.0-2
- rhbz#1001998 - added a workaround for system notifications

* Mon Dec  9 2013 Jan Horak <jhorak@redhat.com> - 24.2.0-1
- Update to 24.2.0

* Sat Nov 02 2013 Dennis Gilmore <dennis@ausil.us> - 24.1.0-2
- remove ExcludeArch: armv7hl

* Wed Oct 30 2013 Jan Horak <jhorak@redhat.com> - 24.1.0-1
- Update to 24.1.0

* Thu Oct 17 2013 Martin Stransky <stransky@redhat.com> - 24.0-4
- Fixed rhbz#1005611 - BEAST workaround not enabled in Firefox

* Wed Sep 25 2013 Jan Horak <jhorak@redhat.com> - 24.0-3
- Update to 24.0

* Mon Sep 23 2013 Jan Horak <jhorak@redhat.com> - 17.0.9-1
- Update to 17.0.9 ESR

* Mon Aug  5 2013 Jan Horak <jhorak@redhat.com> - 17.0.8-1
- Update to 17.0.8

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 17.0.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Tue Jun 25 2013 Jan Horak <jhorak@redhat.com> - 17.0.7-1
- Update to 17.0.7

* Wed Jun 12 2013 Jan Horak <jhorak@redhat.com> - 17.0.6-2
- Fixed rhbz#973371 - unable to install addons

* Tue May 14 2013 Jan Horak <jhorak@redhat.com> - 17.0.6-1
- Update to 17.0.6

* Tue Apr  2 2013 Jan Horak <jhorak@redhat.com> - 17.0.5-1
- Update to 17.0.5

* Mon Mar 11 2013 Jan Horak <jhorak@redhat.com> - 17.0.4-1
- Update to 17.0.4

* Tue Feb 19 2013 Jan Horak <jhorak@redhat.com> - 17.0.3-1
- Update to 17.0.3

* Fri Feb 15 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 17.0.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Tue Jan 15 2013 Martin Stransky <stransky@redhat.com> - 17.0.2-3
- Added fix for NM regression (mozbz#791626)

* Tue Jan 15 2013 Jan Horak <jhorak@redhat.com> - 17.0.2-2
- Added mozilla-746112 patch to fix crash on ppc(64)

* Thu Jan 10 2013 Jan Horak <jhorak@redhat.com> - 17.0.2-1
- Update to 17.0.2

* Mon Nov 19 2012 Jan Horak <jhorak@redhat.com> - 17.0-1
- Update to 17.0

* Mon Oct 29 2012 Jan Horak <jhorak@redhat.com> - 16.0.2-1
- Update to 16.0.2

* Tue Oct 16 2012 Jan Horak <jhorak@redhat.com> - 16.0.1-2
- Fixed nss and nspr versions

* Thu Oct 11 2012 Jan Horak <jhorak@redhat.com> - 16.0.1-1
- Update to 16.0.1

* Tue Oct  9 2012 Jan Horak <jhorak@redhat.com> - 16.0-1
- Update to 16.0

* Tue Sep 18 2012 Dan Horák <dan[at]danny.cz> - 15.0.1-3
- Added fix for rhbz#855923 - TB freezes on Fedora 18 for PPC64

* Fri Sep 14 2012 Martin Stransky <stransky@redhat.com> - 15.0.1-2
- Added build flags for second arches

* Tue Sep 11 2012 Jan Horak <jhorak@redhat.com> - 15.0.1-1
- Update to 15.0.1

* Fri Sep  7 2012 Jan Horak <jhorak@redhat.com> - 15.0-2
- Added workaround fix for PPC (rbhz#852698)

* Mon Aug 27 2012 Jan Horak <jhorak@redhat.com> - 15.0-1
- Update to 15.0

* Wed Aug 1 2012 Martin Stransky <stransky@redhat.com> - 14.0-4
- Removed StartupWMClass (rhbz#844863)
- Fixed -g parameter
- Removed thunderbird-devel before packing to avoid debugsymbols duplicities (rhbz#823940)

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 14.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue Jul 17 2012 Jan Horak <jhorak@redhat.com> - 14.0-1
- Update to 14.0

* Fri Jun 15 2012 Jan Horak <jhorak@redhat.com> - 13.0.1-1
- Update to 13.0.1

* Tue Jun  5 2012 Jan Horak <jhorak@redhat.com> - 13.0-1
- Update to 13.0

* Mon May 7 2012 Martin Stransky <stransky@redhat.com> - 12.0.1-2
- Fixed #717245 - adhere Static Library Packaging Guidelines

* Mon Apr 30 2012 Jan Horak <jhorak@redhat.com> - 12.0.1-1
- Update to 12.0.1

* Tue Apr 24 2012 Jan Horak <jhorak@redhat.com> - 12.0-1
- Update to 12.0

* Mon Apr 16 2012 Peter Robinson <pbrobinson@fedoraproject.org> - 11.0.1-2
- Add upstream patch to fix FTBFS on ARM

* Thu Mar 29 2012 Jan Horak <jhorak@redhat.com> - 11.0.1-1
- Update to 11.0.1

* Thu Mar 22 2012 Jan Horak <jhorak@redhat.com> - 11.0-6
- Added translations to thunderbird.desktop file

* Fri Mar 16 2012 Martin Stransky <stransky@redhat.com> - 11.0-5
- gcc 4.7 build fixes

* Wed Mar 14 2012 Peter Robinson <pbrobinson@fedoraproject.org> - 11.0-4
- Add ARM configuration options

* Wed Mar 14 2012 Martin Stransky <stransky@redhat.com> - 11.0-3
- Build with system libvpx

* Tue Mar 13 2012 Martin Stransky <stransky@redhat.com> - 11.0-1
- Update to 11.0

* Thu Feb 23 2012 Jan Horak <jhorak@redhat.com> - 10.0.1-3
- Added fix for proxy settings mozbz#682832

* Thu Feb 16 2012 Martin Stransky <stransky@redhat.com> - 10.0.1-2
- Added fix for mozbz#727401

* Thu Feb  9 2012 Jan Horak <jhorak@redhat.com> - 10.0.1-1
- Update to 10.0.1

* Mon Feb 6 2012 Martin Stransky <stransky@redhat.com> - 10.0-2
- gcc 4.7 build fixes

* Tue Jan 31 2012 Jan Horak <jhorak@redhat.com> - 10.0-1
- Update to 10.0

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 9.0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Thu Jan 05 2012 Dan Horák <dan[at]danny.cz> - 9.0-6
- disable jemalloc on s390(x) (taken from xulrunner)

* Wed Jan 04 2012 Dan Horák <dan[at]danny.cz> - 9.0-5
- fix build on secondary arches (cherry-picked from 13afcd4c097c)

* Thu Dec 22 2011 Jan Horak <jhorak@redhat.com> - 9.0-4
- Update to 9.0

* Fri Dec 9 2011 Martin Stransky <stransky@redhat.com> - 8.0-4
- enabled gio support (#760644)

* Tue Nov 29 2011 Jan Horak <jhorak@redhat.com> - 8.0-3
- Fixed s390x issues

* Thu Nov 10 2011 Jan Horak <jhorak@redhat.com> - 8.0-2
- Enable Mozilla's crash reporter again for all archs
- Temporary workaround for langpacks
- Disabled addon check UI (#753551)

* Tue Nov  8 2011 Jan Horak <jhorak@redhat.com> - 8.0-1
- Update to 8.0

* Tue Oct 18 2011 Martin Stransky <stransky@redhat.com> - 7.0.1-3
- Added NM patches (mozbz#627672, mozbz#639959)

* Wed Oct 12 2011 Dan Horák <dan[at]danny.cz> - 7.0.1-2
- fix build on secondary arches (copied from xulrunner)

* Fri Sep 30 2011 Jan Horak <jhorak@redhat.com> - 7.0.1-1
- Update to 7.0.1

* Tue Sep 27 2011 Jan Horak <jhorak@redhat.com> - 7.0-1
- Update to 7.0

* Tue Sep  6 2011 Jan Horak <jhorak@redhat.com> - 6.0.2-1
- Update to 6.0.2

* Wed Aug 31 2011 Jan Horak <jhorak@redhat.com> - 6.0-3
- Distrust a specific Certificate Authority

* Wed Aug 31 2011 Dan Horák <dan[at]danny.cz> - 6.0-2
- add secondary-ipc patch from xulrunner

* Tue Aug 16 2011 Jan Horak <jhorak@redhat.com> - 6.0-1
- Update to 6.0

* Tue Aug 16 2011 Remi Collet <remi@fedoraproject.org> 5.0-4
- Don't unzip the langpacks

* Mon Aug 15 2011 Jan Horak <jhorak@redhat.com> - 5.0-3
- Rebuild due to rhbz#728707

* Wed Jul 20 2011 Dan Horák <dan[at]danny.cz> - 5.0-2
- add xulrunner patches for secondary arches

* Tue Jun 28 2011 Jan Horak <jhorak@redhat.com> - 5.0-1
- Update to 5.0

* Tue Jun 21 2011 Jan Horak <jhorak@redhat.com> - 3.1.11-1
- Update to 3.1.11

* Wed May 25 2011 Caolán McNamara <caolanm@redhat.com> - 3.1.10-2
- rebuild for new hunspell

* Thu Apr 28 2011 Jan Horak <jhorak@redhat.com> - 3.1.10-1
- Update to 3.1.10

* Thu Apr 21 2011 Christopher Aillon <caillon@redhat.com> - 3.1.9-7
- Make gvfs-open launch a compose window (salimma)
- Spec file cleanups (salimma, caillon)
- Split out mozilla crashreporter symbols to its own debuginfo package (caillon)

* Sat Apr  2 2011 Christopher Aillon <caillon@redhat.com> - 3.1.9-6
- Drop gio support: the code hooks don't exist yet for TB 3.1.x

* Fri Apr  1 2011 Orion Poplawski <orion@cora.nwra.com> - 3.1.9-5
- Enable startup notification

* Sun Mar 20 2011 Dan Horák <dan[at]danny.cz> - 3.1.9-4
- updated the s390 build patch

* Fri Mar 18 2011 Jan Horak <jhorak@redhat.com> - 3.1.9-3
- Removed gnome-vfs2, libgnomeui and libgnome from build requires

* Wed Mar  9 2011 Jan Horak <jhorak@redhat.com> - 3.1.9-2
- Disabled gnomevfs, enabled gio

* Mon Mar  7 2011 Jan Horak <jhorak@redhat.com> - 3.1.9-1
- Update to 3.1.9

* Tue Mar  1 2011 Jan Horak <jhorak@redhat.com> - 3.1.8-3
- Update to 3.1.8

* Wed Feb  9 2011 Christopher Aillon <caillon@redhat.com> - 3.1.7-6
- Drop the -lightning subpackage, it needs to be in its own SRPM

* Mon Feb  7 2011 Christopher Aillon <caillon@redhat.com> - 3.1.7-5
- Bring back the default mailer check but fix up the directory

* Wed Dec 15 2010 Jan Horak <jhorak@redhat.com> - 3.1.7-4
- Mozilla crash reporter enabled

* Thu Dec  9 2010 Jan Horak <jhorak@redhat.com> - 3.1.7-2
- Fixed useragent

* Thu Dec  9 2010 Jan Horak <jhorak@redhat.com> - 3.1.7-1
- Update to 3.1.7

* Sat Nov 27 2010 Remi Collet <fedora@famillecollet.com> - 3.1.6-8
- fix cairo + nspr required version
- lightning: fix thunderbird version required
- lightning: fix release (b3pre)
- lightning: clean install

* Mon Nov 22 2010 Jan Horak <jhorak@redhat.com> - 3.1.6-7
- Added x-scheme-handler/mailto to thunderbird.desktop file

* Mon Nov  8 2010 Jan Horak <jhorak@redhat.com> - 3.1.6-4
- Added libnotify patch
- Removed dependency on static libraries

* Fri Oct 29 2010 Jan Horak <jhorak@redhat.com> - 3.1.6-2
- Move thunderbird-lightning extension from Sunbird package to Thunderbird

* Wed Oct 27 2010 Jan Horak <jhorak@redhat.com> - 3.1.6-1
- Update to 3.1.6

* Tue Oct 19 2010 Jan Horak <jhorak@redhat.com> - 3.1.5-1
- Update to 3.1.5

* Thu Sep 16 2010 Dan Horák <dan[at]danny.cz> - 3.1.3-2
- fix build on s390

* Tue Sep  7 2010 Jan Horak <jhorak@redhat.com> - 3.1.3-1
- Update to 3.1.3

* Fri Aug  6 2010 Jan Horak <jhorak@redhat.com> - 3.1.2-1
- Update to 3.1.2
- Disable updater

* Tue Jul 20 2010 Jan Horak <jhorak@redhat.com> - 3.1.1-1
- Update to 3.1.1

* Thu Jun 24 2010 Jan Horak <jhorak@redhat.com> - 3.1-1
- Thunderbird 3.1

* Fri Jun 11 2010 Jan Horak <jhorak@redhat.com> - 3.1-0.3.rc2
- TryExec added to desktop file

* Wed Jun  9 2010 Christopher Aillon <caillon@redhat.com> 3.1-0.2.rc2
- Thunderbird 3.1 RC2

* Tue May 25 2010 Christopher Aillon <caillon@redhat.com> 3.1-0.1.rc1
- Thunderbird 3.1 RC1

* Fri Apr 30 2010 Jan Horak <jhorak@redhat.com> - 3.0.4-3
- Fix for mozbz#550455

* Tue Apr 13 2010 Martin Stransky <stransky@redhat.com> - 3.0.4-2
- Fixed langpacks (#580444)

* Tue Mar 30 2010 Jan Horak <jhorak@redhat.com> - 3.0.4-1
- Update to 3.0.4

* Sat Mar 06 2010 Kalev Lember <kalev@smartlink.ee> - 3.0.3-2
- Own extension directories (#532132)

* Mon Mar  1 2010 Jan Horak <jhorak@redhat.com> - 3.0.3-1
- Update to 3.0.3

* Thu Feb 25 2010 Jan Horak <jhorak@redhat.com> - 3.0.2-1
- Update to 3.0.2

* Wed Jan 20 2010 Martin Stransky <stransky@redhat.com> - 3.0.1-1
- Update to 3.0.1

* Mon Jan 18 2010 Martin Stransky <stransky@redhat.com> - 3.0-5
- Added fix for #480603 - thunderbird takes
  unacceptably long time to start

* Wed Dec  9 2009 Jan Horak <jhorak@redhat.com> - 3.0-4
- Update to 3.0

* Thu Dec  3 2009 Jan Horak <jhorak@redhat.com> - 3.0-3.13.rc2
- Update to RC2

* Wed Nov 25 2009 Jan Horak <jhorak@redhat.com> - 3.0-3.12.rc1
- Sync with Mozilla latest RC1 build

* Thu Nov 19 2009 Jan Horak <jhorak@redhat.com> - 3.0-3.11.rc1
- Update to RC1

* Thu Sep 17 2009 Christopher Aillon <caillon@redhat.com> - 3.0-3.9.b4
- Update to 3.0 b4

* Thu Aug  6 2009 Martin Stransky <stransky@redhat.com> - 3.0-3.8.beta3
- Added fix for #437596
- Removed unused patches

* Thu Aug  6 2009 Jan Horak <jhorak@redhat.com> - 3.0-3.7.beta3
- Removed unused build requirements

* Mon Aug  3 2009 Jan Horak <jhorak@redhat.com> - 3.0-3.6.beta3
- Build with system hunspell

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.0-3.5.b3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Jul 24 2009 Jan Horak <jhorak@redhat.com> - 3.0-2.5.beta3
- Use system hunspell

* Tue Jul 21 2009 Jan Horak <jhorak@redhat.com> - 3.0-2.4.beta3
- Update to 3.0 beta3

* Mon Mar 30 2009 Jan Horak <jhorak@redhat.com> - 3.0-2.2.beta2
- Fixed open-browser.sh to use xdg-open instead of gnome-open

* Mon Mar 23 2009 Christopher Aillon <caillon@redhat.com> - 3.0-2.1.beta2
- Disable the default app nag dialog

* Tue Mar 17 2009 Jan Horak <jhorak@redhat.com> - 3.0-2.beta2
- Fixed clicked link does not open in browser (#489120)
- Fixed missing help in thunderbird (#488885)

* Mon Mar  2 2009 Jan Horak <jhorak@redhat.com> - 3.0-1.beta2
- Update to 3.0 beta2
- Added Patch2 to build correctly when building with --enable-shared option

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.0.18-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Jan 07 2009 Christopher Aillon <caillon@redhat.com> - 2.0.0.18-2
- Disable the crash dialog

* Wed Nov 19 2008 Christopher Aillon <caillon@redhat.com> 2.0.0.18-1
- Update to 2.0.0.18

* Thu Oct  9 2008 Christopher Aillon <caillon@redhat.com> 2.0.0.17-1
- Update to 2.0.0.17

* Wed Jul 23 2008 Christopher Aillon <caillon@redhat.com> 2.0.0.16-1
- Update to 2.0.0.16

* Thu May  1 2008 Christopher Aillon <caillon@redhat.com> 2.0.0.14-1
- Update to 2.0.0.14
- Use the system dictionaries

* Fri Apr 18 2008 Christopher Aillon <caillon@redhat.com> 2.0.0.12-6
- Icon belongs in _datadir/pixmaps

* Fri Apr 18 2008 Christopher Aillon <caillon@redhat.com> 2.0.0.12-5
- rebuilt

* Mon Apr  7 2008 Christopher Aillon <caillon@redhat.com> 2.0.0.12-4
- Add %%lang attributes to langpacks

* Sat Mar 15 2008 Christopher Aillon <caillon@redhat.com> 2.0.0.12-3
- Avoid conflict between gecko debuginfos

* Mon Mar 03 2008 Martin Stransky <stransky@redhat.com> 2.0.0.12-2
- Updated starting script (#426331)

* Tue Feb 26 2008 Christopher Aillon <caillon@redhat.com> 2.0.0.12-1
- Update to 2.0.0.12
- Fix up icon location and some scriptlets

* Sun Dec  9 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.9-2
- Fix some rpmlint warnings
- Drop some old patches and obsoletes

* Thu Nov 15 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.9-1
- Update to 2.0.0.9

* Wed Sep 26 2007 Martin Stransky <stransky@redhat.com> 2.0.0.6-6
- Fixed #242657 - firefox -g doesn't work

* Tue Sep 25 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.6-5
- Removed hardcoded MAX_PATH, PATH_MAX and MAXPATHLEN macros

* Tue Sep 11 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.6-4
- Fix crashes when using GTK+ themes containing a gtkrc which specify
  GtkOptionMenu::indicator_size and GtkOptionMenu::indicator_spacing

* Mon Sep 10 2007 Martin Stransky <stransky@redhat.com> 2.0.0.6-3
- added fix for #246248 - firefox crashes when searching for word "do"

* Mon Aug 13 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.6-2
- Update the license tag

* Wed Aug  8 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.6-1
- Update to 2.0.0.6
- Own the application directory (#244901)

* Tue Jul 31 2007 Martin Stransky <stransky@redhat.com> 2.0.0.0-3
- added pango ligature fix

* Thu Apr 19 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.0-1
- Update to 2.0.0.0 Final

* Fri Apr 13 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.0-0.5.rc1
- Fix the desktop file
- Clean up the files list
- Remove the default client stuff from the pref window

* Thu Apr 12 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.0-0.4.rc1
- Rebuild into Fedora

* Wed Apr 11 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.0-0.3.rc1
- Update langpacks

* Thu Apr  5 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.0-0.2.rc1
- Build option tweaks
- Bring the install section to parity with Firefox's

* Thu Apr  5 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.0-0.1.rc1
- Update to 2.0.0.0 RC1

* Sun Mar 25 2007 Christopher Aillon <caillon@redhat.com> 1.5.0.11-1
- Update to 1.5.0.11

* Fri Mar 2 2007 Martin Stransky <stransky@redhat.com> 1.5.0.10-1
- Update to 1.5.0.10

* Mon Feb 12 2007 Martin Stransky <stransky@redhat.com> 1.5.0.9-8
- added fix for #227406: garbage characters on some websites
  (when pango is disabled)

* Tue Jan 30 2007 Christopher Aillon <caillon@redhat.com> 1.5.0.9-7
- Updated cursor position patch from tagoh to fix issue with "jumping"
  cursor when in a textfield with tabs.

* Tue Jan 30 2007 Christopher Aillon <caillon@redhat.com> 1.5.0.9-6
- Fix the DND implementation to not grab, so it works with new GTK+.

* Thu Dec 21 2006 Behdad Esfahbod <besfahbo@redhat.com> 1.5.0.9-5
- Added firefox-1.5-pango-underline.patch

* Wed Dec 20 2006 Behdad Esfahbod <besfahbo@redhat.com> 1.5.0.9-4
- Added firefox-1.5-pango-justified-range.patch

* Tue Dec 19 2006 Behdad Esfahbod <besfahbo@redhat.com> 1.5.0.9-3
- Added firefox-1.5-pango-cursor-position-more.patch

* Tue Dec 19 2006 Matthias Clasen <mclasen@redhat.com> 1.5.0.9-2
- Add a Requires: launchmail  (#219884)

* Tue Dec 19 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.9-1
- Update to 1.5.0.9
- Take firefox's pango fixes
- Don't offer to import...nothing.

* Tue Nov  7 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.8-1
- Update to 1.5.0.8
- Allow choosing of download directory
- Take the user to the correct directory from the Download Manager.
- Patch to add support for printing via pango from Behdad.

* Sun Oct  8 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.7-4
- Default to use of system colors

* Wed Oct  4 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.7-3
- Bring the invisible character to parity with GTK+

* Wed Sep 27 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.7-2
- Fix crash when changing gtk key theme
- Prevent UI freezes while changing GNOME theme
- Remove verbiage about pango; no longer required by upstream.

* Wed Sep 13 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.7-1
- Update to 1.5.0.7

* Thu Sep  7 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.5-8
- Shuffle order of the install phase around

* Thu Sep  7 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.5-7
- Let there be art for Alt+Tab again
- s/tbdir/mozappdir/g

* Wed Sep  6 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.5-6
- Fix for cursor position in editor widgets by tagoh and behdad (#198759)

* Tue Sep  5 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.5-5
- Update nopangoxft.patch
- Fix rendering of MathML thanks to Behdad Esfahbod.
- Update start page text to reflect the MathML fixes.
- Enable pango by default on all locales
- Build using -rpath
- Re-enable GCC visibility

* Thu Aug  3 2006 Kai Engert <kengert@redhat.com> - 1.5.0.5-4
- Fix a build failure in mailnews mime code.

* Tue Aug  1 2006 Matthias Clasen <mclasen@redhat.com> - 1.5.0.5-3
- Rebuild

* Thu Jul 27 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.5-2
- Update to 1.5.0.5

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.5.0.4-2.1
- rebuild

* Mon Jun 12 2006 Kai Engert <kengert@redhat.com> - 1.5.0.4-2
- Update to 1.5.0.4
- Fix desktop-file-utils requires

* Wed Apr 19 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.2-2
- Update to 1.5.0.2

* Thu Mar 16 2006 Christopher Aillon <caillon@redhat.com> - 1.5-7
- Bring the other arches back

* Mon Mar 13 2006 Christopher Aillon <caillon@redhat.com> - 1.5.6
- Temporarily disable other arches that we don't ship FC5 with, for time

* Mon Mar 13 2006 Christopher Aillon <caillon@redhat.com> - 1.5-5
- Add a notice to the mail start page denoting this is a pango enabled build.

* Fri Feb 10 2006 Christopher Aillon <caillon@redhat.com> - 1.5-3
- Add dumpstack.patch
- Improve the langpack install stuff

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.5-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Jan 27 2006 Christopher Aillon <caillon@redhat.com> - 1.5-2
- Add some langpacks back in
- Stop providing MozillaThunderbird

* Thu Jan 12 2006 Christopher Aillon <caillon@redhat.com> - 1.5-1
- Official 1.5 release is out

* Wed Jan 11 2006 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.6.rc1
- Fix crash when deleting highlighted text while composing mail within
  plaintext editor with spellcheck enabled.

* Tue Jan  3 2006 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.5.rc1
- Looks like we can build on ppc64 again.

* Fri Dec 16 2005 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.4.rc1
- Rebuild

* Fri Dec 16 2005 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.3.rc1
- Once again, disable ppc64 because of a new issue.
  See https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=175944

- Use the system NSS libraries
- Build on ppc64

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Nov 28 2005 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.1.rc1
- Fix issue with popup dialogs and other actions causing lockups

* Sat Nov  5 2005 Christopher Aillon <caillon@redhat.com> 1.5-0.5.0.rc1
- Update to 1.5 rc1

* Sat Oct  8 2005 Christopher Aillon <caillon@redhat.com> 1.5-0.5.0.beta2
- Update to 1.5 beta2

* Wed Sep 28 2005 Christopher Aillon <caillon@redhat.com> 1.5-0.5.0.beta1
- Update to 1.5 beta1
- Bring the install phase of the spec file up to speed

* Sun Aug 14 2005 Christopher Aillon <caillon@redhat.com> 1.0.6-4
- Rebuild

* Sat Aug  6 2005 Christopher Aillon <caillon@redhat.com> 1.0.6-3
- Add patch to make file chooser dialog modal

* Fri Jul 22 2005 Christopher Aillon <caillon@redhat.com> 1.0.6-2
- Update to 1.0.6

* Mon Jul 18 2005 Christopher Aillon <caillon@redhat.com> 1.0.6-0.1.fc5
- 1.0.6 Release Candidate

* Fri Jul 15 2005 Christopher Aillon <caillon@redhat.com> 1.0.2-8
- Use system NSPR
- Fix crash on 64bit platforms (#160330)

* Thu Jun 23 2005 Kristian Høgsberg <krh@redhat.com>  1.0.2-7
- Add firefox-1.0-pango-cairo.patch to get rid of the last few Xft
  references, fixing the "no fonts" problem.

* Fri May 13 2005 Christopher Aillon <caillon@redhat.com> 1.0.2-6
- Change the Exec line in the desktop file to `thunderbird`

* Fri May 13 2005 Christopher Aillon <caillon@redhat.com> 1.0.2-5
- Update pango patche, MOZ_DISABLE_PANGO now works as advertised.

* Mon May  9 2005 Christopher Aillon <caillon@redhat.com> 1.0.2-4
- Add temporary workaround to not create files in the user's $HOME (#149664)

* Wed May  4 2005 Christopher Aillon <caillon@redhat.com> 1.0.2-3
- Don't have downloads "disappear" when downloading to desktop (#139015)
- Fix for some more cursor issues in textareas (149991, 150002, 152089)
- Add upstream patch to fix bidi justification of pango
- Add patch to fix launching of helper applications
- Add patch to properly link against libgfxshared_s.a
- Fix multilib conflicts

* Wed Apr 27 2005 Warren Togami <wtogami@redhat.com>
- correct confusing PANGO vars in startup script

* Wed Mar 23 2005 Christopher Aillon <caillon@redhat.com> 1.0.2-1
- Thunderbird 1.0.2

* Tue Mar  8 2005 Christopher Aillon <caillon@redhat.com> 1.0-5
- Add patch to compile against new fortified glibc macros

* Sat Mar  5 2005 Christopher Aillon <caillon@redhat.com> 1.0-4
- Rebuild against GCC 4.0
- Add execshield patches
- Minor specfile cleanup

* Mon Dec 20 2004 Christopher Aillon <caillon@redhat.com> 1.0-3
- Rebuild

* Thu Dec 16 2004 Christopher Aillon <caillon@redhat.com> 1.0-2
- Add RPM version to useragent

* Thu Dec 16 2004 Christopher Blizzard <blizzard@redhat.com>
- Port over pango patches from firefox

* Wed Dec  8 2004 Christopher Aillon <caillon@redhat.com> 1.0-1
- Thunderbird 1.0

* Mon Dec  6 2004 Christopher Aillon <caillon@redhat.com> 1.0-0.rc1.1
- Fix advanced prefs

* Fri Dec  3 2004 Christopher Aillon <caillon@redhat.com>
- Make this run on s390(x) now for real

* Wed Dec  1 2004 Christopher Aillon <caillon@redhat.com> 1.0-0.rc1.0
- Update to 1.0 rc1

* Fri Nov 19 2004 Christopher Aillon <caillon@redhat.com>
- Add patches to build and run on s390(x)

* Thu Nov 11 2004 Christopher Aillon <caillon@redhat.com> 0.9.0-2
- Rebuild to fix file chooser

* Fri Nov  5 2004 Christopher Aillon <caillon@redhat.com> 0.9.0-1
- Update to 0.9

* Fri Oct 22 2004 Christopher Aillon <caillon@redhat.com> 0.8.0-10
- Prevent inlining of stack direction detection (#135255)

* Tue Oct 19 2004 Christopher Aillon <caillon@redhat.com> 0.8.0-9
- More file chooser fixes (same as in firefox)
- Fix for upstream 28327.

* Mon Oct 18 2004 Christopher Blizzard <blizzard@redhat.com> 0.8.0-8
- Update the pango patch

* Mon Oct 18 2004 Christopher Blizzard <blizzard@redhat.com> 0.8.0-8
- Pull over patches from firefox build:
  - disable default application dialog
  - don't include software update since it doesn't work
  - make external app support work

* Thu Oct 14 2004 Christopher Blizzard <blizzard@redhat.com> 0.8.0-7
- Use pango for rendering

* Tue Oct 12 2004 Christopher Aillon <caillon@redhat.com> 0.8.0-6
- Fix for 64 bit crash at startup (b.m.o #256603)

* Sat Oct  9 2004 Christopher Aillon <caillon@redhat.com> 0.8.0-5
- Add patches to fix xremote (#135036)

* Fri Oct  8 2004 Christopher Aillon <caillon@redhat.com> 0.8.0-4
- Add patch to fix button focus issues (#133507)
- Add patch for fix IMAP race issues (bmo #246439)

* Fri Oct  1 2004 Bill Nottingham <notting@redhat.com> 0.8.0-3
- filter out library Provides: and internal Requires:

* Tue Sep 28 2004 Christopher Aillon <caillon@redhat.com> 0.8.0-2
- Backport the GTK+ File Chooser.
- Add fix for JS math on x86_64 systems
- Add pkgconfig patch

* Thu Sep 16 2004 Christopher Aillon <caillon@redhat.com> 0.8.0-1
- Update to 0.8.0
- Remove enigmail
- Update BuildRequires
- Remove gcc34 and extension manager patches -- they are upstreamed.
- Fix for gnome-vfs2 error at component registration

* Fri Sep 03 2004 Christopher Aillon <caillon@redhat.com> 0.7.3-5
- Build with --disable-xprint

* Wed Sep 01 2004 David Hill <djh[at]ii.net> 0.7.3-4
- remove all Xvfb-related hacks

* Wed Sep 01 2004 Warren Togami <wtogami@redhat.com>
- actually apply psfonts
- add mozilla gnome-uriloader patch to prevent build failure

* Tue Aug 31 2004 Warren Togami <wtogami@redhat.com> 0.7.3-3
- rawhide import
- apply NetBSD's freetype 2.1.8 patch
- apply psfonts patch
- remove BR on /usr/bin/ex, breaks beehive

* Tue Aug 31 2004 David Hill <djh[at]ii.net> 0.7.3-0.fdr.2
- oops, fix %%install

* Thu Aug 26 2004 David Hill <djh[at]ii.net> 0.7.3-0.fdr.1
- update to Thunderbird 0.7.3 and Enigmail 0.85.0
- remove XUL.mfasl on startup, add Debian enigmail patches
- add Xvfb hack for -install-global-extension

* Wed Jul 14 2004 David Hill <djh[at]ii.net> 0.7.2-0.fdr.0
- update to 0.7.2, just because it's there
- update gcc-3.4 patch (Kaj Niemi)
- add EM registration patch and remove instdir hack

* Sun Jul 04 2004 David Hill <djh[at]ii.net> 0.7.1-0.fdr.1
- re-add Enigmime 1.0.7, omit Enigmail until the Mozilla EM problems are fixed

* Wed Jun 30 2004 David Hill <djh[at]ii.net> 0.7.1-0.fdr.0
- update to 0.7.1
- remove Enigmail

* Mon Jun 28 2004 David Hill <djh[at]ii.net> 0.7-0.fdr.1
- re-enable Enigmail 0.84.1
- add gcc-3.4 patch (Kaj Niemi)
- use official branding (with permission)

* Fri Jun 18 2004 David Hill <djh[at]ii.net> 0.7-0.fdr.0
- update to 0.7
- temporarily disable Enigmail 0.84.1, make ftp links work (#1634)
- specify libdir, change BR for apt (V. Skyttä, #1617)

* Tue May 18 2004 Warren Togami <wtogami@redhat.com> 0.6-0.fdr.5
- temporary workaround for enigmail skin "modern" bug

* Mon May 10 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.4
- update to Enigmail 0.84.0
- update launch script

* Mon May 10 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.3
- installation directory now versioned
- allow root to run the program (for installing extensions)
- remove unnecessary %%pre and %%post
- remove separators, update mozconfig and launch script (M. Schwendt, #1460)

* Wed May 05 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.2
- include Enigmail, re-add release notes
- delete %%{_libdir}/thunderbird in %%pre

* Mon May 03 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.1
- update to Thunderbird 0.6

* Fri Apr 30 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.0.rc1
- update to Thunderbird 0.6 RC1
- add new icon, remove release notes

* Thu Apr 15 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.0.20040415
- update to latest CVS, update mozconfig and %%build accordingly
- update to Enigmail 0.83.6
- remove x-remote and x86_64 patches
- build with -Os

* Thu Apr 15 2004 David Hill <djh[at]ii.net> 0.5-0.fdr.12
- update x-remote patch
- more startup script fixes

* Tue Apr 06 2004 David Hill <djh[at]ii.net> 0:0.5-0.fdr.11
- startup script fixes, and a minor cleanup

* Sun Apr 04 2004 Warren Togami <wtogami@redhat.com> 0:0.5-0.fdr.10
- Minor cleanups

* Sun Apr 04 2004 David Hill <djh[at]ii.net> 0:0.5-0.fdr.8
- minor improvements to open-browser.sh and startup script
- update to latest version of Blizzard's x-remote patch

* Thu Mar 25 2004 David Hill <djh[at]ii.net> 0:0.5-0.fdr.7
- update open-browser.sh, startup script, and BuildRequires

* Sun Mar 14 2004 David Hill <djh[at]ii.net> 0:0.5-0.fdr.6
- update open-browser script, modify BuildRequires (Warren)
- add Blizzard's x-remote patch
- initial attempt at x-remote-enabled startup script

* Sun Mar 07 2004 David Hill <djh[at]ii.net> 0:0.5-0.fdr.5
- refuse to run with excessive privileges

* Fri Feb 27 2004 David Hill <djh[at]ii.net> 0:0.5-0.fdr.4
- add Mozilla x86_64 patch (Oliver Sontag)
- Enigmail source filenames now include the version
- modify BuildRoot

* Thu Feb 26 2004 David Hill <djh[at]ii.net> 0:0.5-0.fdr.3
- use the updated official tarball

* Wed Feb 18 2004 David Hill <djh[at]ii.net> 0:0.5-0.fdr.2
- fix %%prep script

* Mon Feb 16 2004 David Hill <djh[at]ii.net> 0:0.5-0.fdr.1
- update Enigmail to 0.83.3
- use official source tarball (after removing the CRLFs)
- package renamed to thunderbird

* Mon Feb 09 2004 David Hill <djh[at]ii.net> 0:0.5-0.fdr.0
- update to 0.5
- check for lockfile before launching

* Fri Feb 06 2004 David Hill <djh[at]ii.net>
- update to latest cvs
- update to Enigmail 0.83.2

* Thu Jan 29 2004 David Hill <djh[at]ii.net> 0:0.4-0.fdr.5
- update to Enigmail 0.83.1
- removed Mozilla/Firebird script patching

* Sat Jan 03 2004 David Hill <djh[at]ii.net> 0:0.4-0.fdr.4
- add startup notification to .desktop file

* Thu Dec 25 2003 Warren Togami <warren@togami.com> 0:0.4-0.fdr.3
- open-browser.sh release 3
- patch broken /usr/bin/mozilla script during install
- dir ownership
- XXX: Source fails build on x86_64... fix later

* Tue Dec 23 2003 David Hill <djh[at]ii.net> 0:0.4-0.fdr.2
- update to Enigmail 0.82.5
- add Warren's open-browser.sh (#1113)

* Tue Dec 09 2003 David Hill <djh[at]ii.net> 0:0.4-0.fdr.1
- use Thunderbird's mozilla-xremote-client to launch browser

* Sun Dec 07 2003 David Hill <djh[at]ii.net> 0:0.4-0.fdr.0
- update to 0.4
- make hyperlinks work (with recent versions of Firebird/Mozilla)

* Thu Dec 04 2003 David Hill <djh[at]ii.net>
- update to 0.4rc2

* Wed Dec 03 2003 David Hill <djh[at]ii.net>
- update to 0.4rc1 and Enigmail 0.82.4

* Thu Nov 27 2003 David Hill <djh[at]ii.net>
- update to latest CVS and Enigmail 0.82.3

* Sun Nov 16 2003 David Hill <djh[at]ii.net>
- update to latest CVS (0.4a)
- update Enigmail to 0.82.2
- alter mozconfig for new build requirements
- add missing BuildReq (#987)

* Thu Oct 16 2003 David Hill <djh[at]ii.net> 0:0.3-0.fdr.0
- update to 0.3

* Sun Oct 12 2003 David Hill <djh[at]ii.net> 0:0.3rc3-0.fdr.0
- update to 0.3rc3
- update Enigmail to 0.81.7

* Thu Oct 02 2003 David Hill <djh[at]ii.net> 0:0.3rc2-0.fdr.0
- update to 0.3rc2

* Wed Sep 17 2003 David Hill <djh[at]ii.net> 0:0.2-0.fdr.2
- simplify startup script

* Wed Sep 10 2003 David Hill <djh[at]ii.net> 0:0.2-0.fdr.1
- add GPG support (Enigmail 0.81.6)
- specfile fixes (#679)

* Thu Sep 04 2003 David Hill <djh[at]ii.net> 0:0.2-0.fdr.0
- update to 0.2

* Mon Sep 01 2003 David Hill <djh[at]ii.net>
- initial RPM
  (based on the fedora MozillaFirebird-0.6.1 specfile)
